# joi Schema.org
This package contains schema validation for [Schema.org](https://schema.org/) types for [joi](https://github.com/hapijs/joi).

## Validation
To keep it simple, only one type is possible.
For instance, if a field accepts URL's or a Thing object, and an array is provided with multiple entries, it then cannot be a mix of URL and Thing objects.

## Reference schema's to prevent infinite loops
To prevent infinite loops, there are reference schema's with an identifier and @type which should reference to a different record in the database to get the complete object.
The idea is to create a builder class that adds the complete objects where needed, or get them through an API for example.

## Multiple values
Certain fields can also be an array with values, for example an Article can have multiple images.
Sometimes it might be hard to figure out if a field can have multiple values or just one.
We did it based on what we think was right, but we can of course be wrong with different use cases.
Feel free to add feedback or create new PR's when needed. 
