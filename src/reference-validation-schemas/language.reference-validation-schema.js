/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  languageReferenceValidationSchema = {
    "@context": Joi.string().pattern(/^https:\/\/schema.org\/$/),
    "@type": Joi.string().pattern(/^Language$/).required(),
    identifier: Joi.string().required()
  };

module.exports = {
  languageReferenceValidationSchema
};
