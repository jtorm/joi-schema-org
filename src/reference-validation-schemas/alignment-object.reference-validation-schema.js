/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  alignmentObjectReferenceValidationSchema = referenceValidationSchemaModel('AlignmentObject');

module.exports = {
  alignmentObjectReferenceValidationSchema
};
