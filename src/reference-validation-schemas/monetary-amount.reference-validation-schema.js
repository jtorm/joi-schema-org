/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  monetaryAmountReferenceValidationSchema = referenceValidationSchemaModel('MonetaryAmount');

module.exports = {
  monetaryAmountReferenceValidationSchema
};
