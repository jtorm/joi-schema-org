/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  {anyThingReferenceValidationSchema} = require("./../reference-validation-schemas/any-thing.reference-validation-schema"),
  {dateValidationSchema} = require('./date.validation-schema'),
  {dateTimeValidationSchema} = require('./date-time.validation-schema'),
  {thingValidationSchema} = require('./thing.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),

  dataFeedItemValidationSchema = validationSchemaModel(
    'DataFeedItem',
    {
      dateCreated: Joi.alternatives(
        dateValidationSchema,
        dateTimeValidationSchema
      ),
      dateDeleted: Joi.alternatives(
        dateValidationSchema,
        dateTimeValidationSchema
      ),
      dateModified: Joi.alternatives(
        dateValidationSchema,
        dateTimeValidationSchema
      ),
      item: anyThingReferenceValidationSchema,

      ...thingValidationSchema
    }
  );

module.exports = {
  dataFeedItemValidationSchema
};
