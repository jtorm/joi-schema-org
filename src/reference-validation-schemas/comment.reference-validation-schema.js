/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  commentReferenceValidationSchema = referenceValidationSchemaModel('Comment');

module.exports = {
  commentReferenceValidationSchema
};
