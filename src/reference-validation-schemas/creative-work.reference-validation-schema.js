/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  creativeWorkReferenceValidationSchema = referenceValidationSchemaModel('CreativeWork');

module.exports = {
  creativeWorkReferenceValidationSchema
};
