/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  educationalOrganizationReferenceValidationSchema = referenceValidationSchemaModel('EducationalOrganization');

module.exports = {
  educationalOrganizationReferenceValidationSchema
};
