/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  {actionStatusTypeValidationSchema} = require('./action-status-type.validation-schema'),
  {anyThingReferenceValidationSchema} = require('./../reference-validation-schemas/any-thing.reference-validation-schema'),
  {dateTimeValidationSchema} = require('./date-time.validation-schema'),
  {entryPointReferenceValidationSchema} = require('./../reference-validation-schemas/entry-point.reference-validation-schema'),
  {organizationReferenceValidationSchema} = require('./../reference-validation-schemas/organization.reference-validation-schema'),
  {personReferenceValidationSchema} = require('./../reference-validation-schemas/person.reference-validation-schema'),
  {placeReferenceValidationSchema} = require('./../reference-validation-schemas/place.reference-validation-schema'),
  {postalAddressReferenceValidationSchema} = require('./../reference-validation-schemas/postal-address.reference-validation-schema'),
  {timeValidationSchema} = require('./time.validation-schema'),
  {textValidationSchema} = require('./text.validation-schema'),
  {thingValidationSchema} = require('./thing.validation-schema'),
  {urlValidationSchema} = require('./url.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),
  {virtualLocationReferenceValidationSchema} = require('./../reference-validation-schemas/virtual-location.reference-validation-schema'),

  actionValidationSchema = validationSchemaModel(
    'Action',
    {
      actionStatus: Joi.alternatives(actionStatusTypeValidationSchema).allow(null),
      agent: Joi.alternatives(
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema),
        organizationReferenceValidationSchema,
        Joi.array().items(organizationReferenceValidationSchema)
      ).allow(null),
      endTime: Joi.alternatives(
        dateTimeValidationSchema,
        timeValidationSchema
      ),
      error: Joi.alternatives(anyThingReferenceValidationSchema).allow(null),
      instrument: Joi.alternatives(anyThingReferenceValidationSchema).allow(null),
      location: Joi.alternatives(
        placeReferenceValidationSchema,
        Joi.array().items(placeReferenceValidationSchema),
        postalAddressReferenceValidationSchema,
        Joi.array().items(postalAddressReferenceValidationSchema),
        textValidationSchema,
        Joi.array().items(textValidationSchema),
        virtualLocationReferenceValidationSchema,
        Joi.array().items(virtualLocationReferenceValidationSchema)
      ).allow(null),
      object: Joi.alternatives(anyThingReferenceValidationSchema).allow(null),
      participant: Joi.alternatives(
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema),
        organizationReferenceValidationSchema,
        Joi.array().items(organizationReferenceValidationSchema)
      ).allow(null),
      provider: Joi.alternatives(
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema),
        organizationReferenceValidationSchema,
        Joi.array().items(organizationReferenceValidationSchema)
      ).allow(null),
      result: Joi.alternatives(anyThingReferenceValidationSchema).allow(null),
      startTime: Joi.alternatives(
        dateTimeValidationSchema,
        timeValidationSchema
      ).allow(null),
      target: Joi.alternatives(
        entryPointReferenceValidationSchema,
        urlValidationSchema
      ).allow(null),

      ...thingValidationSchema
    }
  );

module.exports = {
  actionValidationSchema
};
