/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  entryPointReferenceValidationSchema = referenceValidationSchemaModel('EntryPoint');

module.exports = {
  entryPointReferenceValidationSchema
};
