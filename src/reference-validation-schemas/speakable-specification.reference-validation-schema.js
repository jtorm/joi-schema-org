/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  speakableSpecificationReferenceValidationSchema = referenceValidationSchemaModel('SpeakableSpecification');

module.exports = {
  speakableSpecificationReferenceValidationSchema
};
