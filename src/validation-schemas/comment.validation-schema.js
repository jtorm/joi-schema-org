/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {creativeWorkValidationSchema} = require('./creative-work.validation-schema'),
  {commentReferenceValidationSchema} = require('./../reference-validation-schemas/comment.reference-validation-schema'),
  {integerValidationSchema} = require('./integer.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),

  commentValidationSchema = validationSchemaModel(
    'Comment',
    {
      downvoteCount: integerValidationSchema,
      parentItem: commentReferenceValidationSchema,
      upvoteCount: integerValidationSchema,

      ...creativeWorkValidationSchema
    }
  );

module.exports = {
  commentValidationSchema
};
