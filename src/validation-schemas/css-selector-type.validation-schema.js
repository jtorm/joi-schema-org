/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  cssSelectorTypeValidationSchema = Joi.string().allow(null);

module.exports = {
  cssSelectorTypeValidationSchema
};
