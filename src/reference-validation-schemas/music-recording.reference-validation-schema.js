/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  musicRecordingReferenceValidationSchema = referenceValidationSchemaModel('MusicRecording');

module.exports = {
  musicRecordingReferenceValidationSchema
};
