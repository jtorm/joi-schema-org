/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  {creativeWorkSeasonReferenceValidationSchema} = require('./../reference-validation-schemas/creative-work-season.reference-validation-schema'),
  {creativeWorkSeriesReferenceValidationSchema} = require('./../reference-validation-schemas/creative-work-series.reference-validation-schema'),
  {creativeWorkValidationSchema} = require('./creative-work.validation-schema'),
  {episodeReferenceValidationSchema} = require('./../reference-validation-schemas/episode.reference-validation-schema'),
  {hyperTocEntryReferenceValidationSchema} = require('./../reference-validation-schemas/hyper-toc-entry.reference-validation-schema'),
  {integerValidationSchema} = require('./integer.validation-schema'),
  {musicGroupReferenceValidationSchema} = require('./../reference-validation-schemas/music-group.reference-validation-schema'),
  {numberValidationSchema} = require('./number.validation-schema'),
  {personReferenceValidationSchema} = require('./../reference-validation-schemas/person.reference-validation-schema'),
  {textValidationSchema} = require('./text.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),

  clipValidationSchema = validationSchemaModel(
    'Clip',
    {
      actor: Joi.alternatives(
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema)
      ).allow(null),
      clipNumber: Joi.alternatives(
        integerValidationSchema,
        textValidationSchema
      ),
      director: Joi.alternatives(
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema)
      ).allow(null),
      endOffset: Joi.alternatives(
        hyperTocEntryReferenceValidationSchema,
        numberValidationSchema
      ),
      musicBy: Joi.alternatives(
        musicGroupReferenceValidationSchema,
        Joi.array().items(musicGroupReferenceValidationSchema),
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema)
      ).allow(null),
      partOfEpisode: Joi.alternatives(episodeReferenceValidationSchema).allow(null),
      partOfSeason: Joi.alternatives(creativeWorkSeasonReferenceValidationSchema).allow(null),
      partOfSeries: Joi.alternatives(creativeWorkSeriesReferenceValidationSchema).allow(null),
      startOffset: Joi.alternatives(
        hyperTocEntryReferenceValidationSchema,
        numberValidationSchema
      ),

      ...creativeWorkValidationSchema
    }
  );

module.exports = {
  clipValidationSchema
};
