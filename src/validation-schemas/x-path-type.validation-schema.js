/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  xPathTypeValidationSchema = Joi.string().allow(null);

module.exports = {
  xPathTypeValidationSchema
};
