/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  {administrativeAreaReferenceValidationSchema} = require('./../reference-validation-schemas/administrative-area.reference-validation-schema'),
  {anyThingReferenceValidationSchema} = require("./../reference-validation-schemas/any-thing.reference-validation-schema"),
  {aggregateRatingReferenceValidationSchema} = require('./../reference-validation-schemas/aggregate-rating.reference-validation-schema'),
  {articleReferenceValidationSchema} = require('./../reference-validation-schemas/article.reference-validation-schema'),
  {brandReferenceValidationSchema} = require('./../reference-validation-schemas/brand.reference-validation-schema'),
  {contactPointReferenceValidationSchema} = require('./../reference-validation-schemas/contact-point.reference-validation-schema'),
  {creativeWorkReferenceValidationSchema} = require('./../reference-validation-schemas/creative-work.reference-validation-schema'),
  {dateValidationSchema} = require('./date.validation-schema'),
  {definedTermReferenceValidationSchema} = require('./../reference-validation-schemas/defined-term.reference-validation-schema'),
  {demandReferenceValidationSchema} = require('./../reference-validation-schemas/demand.reference-validation-schema'),
  {educationalOccupationalCredentialReferenceValidationSchema} = require('./../reference-validation-schemas/educational-occupational-credential.reference-validation-schema'),
  {eventReferenceValidationSchema} = require('./../reference-validation-schemas/event.reference-validation-schema'),
  {geoShapeReferenceValidationSchema} = require('./../reference-validation-schemas/geo-shape.reference-validation-schema'),
  {grantReferenceValidationSchema} = require('./../reference-validation-schemas/grant.reference-validation-schema'),
  {imageObjectReferenceValidationSchema} = require("../reference-validation-schemas/image-object.reference-validation-schema"),
  {interactionCounterReferenceValidationSchema} = require('./../reference-validation-schemas/interaction-counter.reference-validation-schema'),
  {languageReferenceValidationSchema} = require('./../reference-validation-schemas/language.reference-validation-schema'),
  {merchantReturnPolicyReferenceValidationSchema} = require('./../reference-validation-schemas/merchant-return-policy.reference-validation-schema'),
  {nonprofitTypeValidationSchema} = require('./nonprofit-type.validation-schema'),
  {offerCatalogReferenceValidationSchema} = require('./../reference-validation-schemas/offer-catalog.reference-validation-schema'),
  {offerReferenceValidationSchema} = require('./../reference-validation-schemas/offer.reference-validation-schema'),
  {organizationReferenceValidationSchema} = require('./../reference-validation-schemas/organization.reference-validation-schema'),
  {ownershipInfoReferenceValidationSchema} = require('./../reference-validation-schemas/ownership-info.reference-validation-schema'),
  {personReferenceValidationSchema} = require('./../reference-validation-schemas/person.reference-validation-schema'),
  {placeReferenceValidationSchema} = require('./../reference-validation-schemas/place.reference-validation-schema'),
  {postalAddressReferenceValidationSchema} = require('./../reference-validation-schemas/postal-address.reference-validation-schema'),
  {productReferenceValidationSchema} = require('./../reference-validation-schemas/product.reference-validation-schema'),
  {programMembershipReferenceValidationSchema} = require('./../reference-validation-schemas/program-membership.reference-validation-schema'),
  {quantitativeValueReferenceValidationSchema} = require('./../reference-validation-schemas/quantitative-value.reference-validation-schema'),
  {reviewReferenceValidationSchema} = require('./../reference-validation-schemas/review.reference-validation-schema'),
  {textValidationSchema} = require('./text.validation-schema'),
  {thingValidationSchema} = require('./thing.validation-schema'),
  {urlValidationSchema} = require('./url.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),
  {virtualLocationReferenceValidationSchema} = require('./../reference-validation-schemas/virtual-location.reference-validation-schema'),

  organizationValidationSchema = validationSchemaModel(
    'Organization',
    {
      actionableFeedbackPolicy: Joi.alternatives(
        creativeWorkReferenceValidationSchema,
        urlValidationSchema
      ),
      address: Joi.alternatives(
        postalAddressReferenceValidationSchema,
        textValidationSchema
      ),
      aggregateRating: Joi.alternatives(aggregateRatingReferenceValidationSchema).allow(null),
      alumni: Joi.alternatives(
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema)
      ).allow(null),
      areaServed: Joi.alternatives(
        administrativeAreaReferenceValidationSchema,
        geoShapeReferenceValidationSchema,
        placeReferenceValidationSchema,
        textValidationSchema
      ),
      award: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema)
      ),
      brand: Joi.alternatives(
        brandReferenceValidationSchema,
        organizationReferenceValidationSchema
      ).allow(null),
      contactPoint: Joi.alternatives(
        contactPointReferenceValidationSchema,
        Joi.array().items(contactPointReferenceValidationSchema)
      ).allow(null),
      correctionsPolicy: Joi.alternatives(
        creativeWorkReferenceValidationSchema,
        urlValidationSchema
      ),
      department: Joi.alternatives(organizationReferenceValidationSchema).allow(null),
      dissolutionDate: dateValidationSchema,
      diversityPolicy: Joi.alternatives(
        creativeWorkReferenceValidationSchema,
        urlValidationSchema
      ),
      diversityStaffingReport: Joi.alternatives(
        articleReferenceValidationSchema,
        urlValidationSchema
      ),
      duns: textValidationSchema,
      email: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema)
      ),
      employee: Joi.alternatives(
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema)
      ).allow(null),
      ethicsPolicy: Joi.alternatives(
        creativeWorkReferenceValidationSchema,
        urlValidationSchema
      ),
      event: Joi.alternatives(
        eventReferenceValidationSchema,
        Joi.array().items(eventReferenceValidationSchema)
      ).allow(null),
      faxNumber: textValidationSchema,
      founder: Joi.alternatives(
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema)
      ).allow(null),
      foundingDate: dateValidationSchema,
      foundingLocation: Joi.alternatives(placeReferenceValidationSchema).allow(null),
      funder: Joi.alternatives(
        organizationReferenceValidationSchema,
        Joi.array().items(organizationReferenceValidationSchema),
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema)
      ).allow(null),
      funding: Joi.alternatives(
        grantReferenceValidationSchema,
        Joi.array().items(grantReferenceValidationSchema)
      ).allow(null),
      globalLocationNumber: textValidationSchema,
      hasCredential: Joi.alternatives(
        educationalOccupationalCredentialReferenceValidationSchema,
        Joi.array().items(educationalOccupationalCredentialReferenceValidationSchema)
      ).allow(null),
      hasMerchantReturnPolicy: Joi.alternatives(merchantReturnPolicyReferenceValidationSchema).allow(null),
      hasOfferCatalog: Joi.alternatives(
        offerCatalogReferenceValidationSchema,
        Joi.array().items(offerCatalogReferenceValidationSchema)
      ).allow(null),
      hasPOS: Joi.alternatives(
        placeReferenceValidationSchema,
        Joi.array().items(placeReferenceValidationSchema)
      ).allow(null),
      interactionStatistic: Joi.alternatives(
        interactionCounterReferenceValidationSchema,
        Joi.array().items(interactionCounterReferenceValidationSchema)
      ).allow(null),
      isicV4: textValidationSchema,
      iso6523Code: textValidationSchema,
      keywords: Joi.alternatives(
        definedTermReferenceValidationSchema,
        Joi.array().items(definedTermReferenceValidationSchema),
        textValidationSchema,
        Joi.array().items(textValidationSchema),
        urlValidationSchema,
        Joi.array().items(urlValidationSchema)
      ),
      knowsAbout: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema),
        anyThingReferenceValidationSchema,
        Joi.array().items(anyThingReferenceValidationSchema),
        urlValidationSchema,
        Joi.array().items(urlValidationSchema)
      ),
      knowsLanguage: Joi.alternatives(
        languageReferenceValidationSchema,
        Joi.array().items(languageReferenceValidationSchema),
        textValidationSchema,
        Joi.array().items(textValidationSchema)
      ),
      legalName: textValidationSchema,
      leiCode: textValidationSchema,
      location: Joi.alternatives(
        placeReferenceValidationSchema,
        Joi.array().items(placeReferenceValidationSchema),
        postalAddressReferenceValidationSchema,
        Joi.array().items(postalAddressReferenceValidationSchema),
        textValidationSchema,
        Joi.array().items(textValidationSchema),
        virtualLocationReferenceValidationSchema,
        Joi.array().items(virtualLocationReferenceValidationSchema)
      ),
      logo: Joi.alternatives(
        imageObjectReferenceValidationSchema,
        Joi.array().items(imageObjectReferenceValidationSchema),
        urlValidationSchema,
        Joi.array().items(urlValidationSchema)
      ),
      makesOffer: Joi.alternatives(
        offerReferenceValidationSchema,
        Joi.array().items(offerReferenceValidationSchema)
      ).allow(null),
      member: Joi.alternatives(
        organizationReferenceValidationSchema,
        Joi.array().items(organizationReferenceValidationSchema),
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema)
      ).allow(null),
      memberOf: Joi.alternatives(
        organizationReferenceValidationSchema,
        Joi.array().items(organizationReferenceValidationSchema),
        programMembershipReferenceValidationSchema,
        Joi.array().items(programMembershipReferenceValidationSchema)
      ).allow(null),
      naics: textValidationSchema,
      nonprofitStatus: Joi.alternatives(nonprofitTypeValidationSchema).allow(null),
      numberOfEmployees: Joi.alternatives(quantitativeValueReferenceValidationSchema).allow(null),
      ownershipFundingInfo: Joi.alternatives(
        anyThingReferenceValidationSchema,
        Joi.array().items(anyThingReferenceValidationSchema),
        textValidationSchema,
        Joi.array().items(textValidationSchema),
        urlValidationSchema,
        Joi.array().items(urlValidationSchema),
      ),
      owns: Joi.alternatives(
        ownershipInfoReferenceValidationSchema,
        Joi.array().items(ownershipInfoReferenceValidationSchema),
        productReferenceValidationSchema,
        Joi.array().items(productReferenceValidationSchema)
      ).allow(null),
      parentOrganization: Joi.alternatives(
        organizationReferenceValidationSchema,
        Joi.array().items(organizationReferenceValidationSchema)
      ).allow(null),
      publishingPrinciples: Joi.alternatives(
        creativeWorkReferenceValidationSchema,
        Joi.array().items(creativeWorkReferenceValidationSchema),
        urlValidationSchema,
        Joi.array().items(urlValidationSchema)
      ),
      review: Joi.alternatives(
        reviewReferenceValidationSchema,
        Joi.array().items(reviewReferenceValidationSchema)
      ).allow(null),
      seeks: Joi.alternatives(
        demandReferenceValidationSchema,
        Joi.array().items(demandReferenceValidationSchema)
      ).allow(null),
      slogan: textValidationSchema,
      sponsor: Joi.alternatives(
        organizationReferenceValidationSchema,
        Joi.array().items(organizationReferenceValidationSchema),
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema)
      ).allow(null),
      subOrganization: Joi.alternatives(
        organizationReferenceValidationSchema,
        Joi.array().items(organizationReferenceValidationSchema)
      ).allow(null),
      taxID: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema)
      ),
      telephone: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema)
      ),
      unnamedSourcesPolicy: Joi.alternatives(
        creativeWorkReferenceValidationSchema,
        urlValidationSchema
      ),
      vatID: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema)
      ),

      ...thingValidationSchema
    }
  );

module.exports = {
  organizationValidationSchema
};
