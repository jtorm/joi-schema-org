/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {socialMediaPostingValidationSchema} = require('./social-media-posting.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),

  blogPostingValidationSchema = validationSchemaModel(
    'BlogPosting',
    socialMediaPostingValidationSchema
  );

module.exports = {
  blogPostingValidationSchema
};
