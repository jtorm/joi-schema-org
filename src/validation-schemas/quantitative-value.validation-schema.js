/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  {booleanValidationSchema} = require('./boolean.validation-schema'),
  {definedTermReferenceValidationSchema} = require('./../reference-validation-schemas/defined-term.reference-validation-schema'),
  {enumerationReferenceValidationSchema} = require('./../reference-validation-schemas/enumeration.reference-validation-schema'),
  {measurementTypeEnumerationValidationSchema} = require('./measurement-type-enumeration.validation-schema'),
  {numberValidationSchema} = require('./number.validation-schema'),
  {propertyValueReferenceValidationSchema} = require("./../reference-validation-schemas/property-value.reference-validation-schema"),
  {qualitativeValueReferenceValidationSchema} = require('./../reference-validation-schemas/qualitative-value.reference-validation-schema'),
  {quantitativeValueReferenceValidationSchema} = require('./../reference-validation-schemas/quantitative-value.reference-validation-schema'),
  {structuredValueReferenceValidationSchema} = require('./../reference-validation-schemas/structured-value.reference-validation-schema'),
  {textValidationSchema} = require('./text.validation-schema'),
  {thingValidationSchema} = require('./thing.validation-schema'),
  {urlValidationSchema} = require('./url.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),

  quantitativeValueValidationSchema = validationSchemaModel(
    'QuantitativeValue',
    {
      "additionalProperty": propertyValueReferenceValidationSchema,
      "maxValue": numberValidationSchema,
      "minValue": numberValidationSchema,
      "unitCode": Joi.alternatives(
        textValidationSchema,
        urlValidationSchema
      ),
      "unitText": textValidationSchema,
      "value": Joi.alternatives(
        booleanValidationSchema,
        numberValidationSchema,
        structuredValueReferenceValidationSchema,
        textValidationSchema
      ),
      "valueReference": Joi.alternatives(
        definedTermReferenceValidationSchema,
        enumerationReferenceValidationSchema,
        measurementTypeEnumerationValidationSchema,
        propertyValueReferenceValidationSchema,
        qualitativeValueReferenceValidationSchema,
        quantitativeValueReferenceValidationSchema,
        structuredValueReferenceValidationSchema,
        textValidationSchema
      ),

      ...thingValidationSchema
    }
  );

module.exports = {
  quantitativeValueValidationSchema
};
