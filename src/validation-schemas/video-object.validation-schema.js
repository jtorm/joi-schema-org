/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  {imageObjectReferenceValidationSchema} = require('./../reference-validation-schemas/image-object.reference-validation-schema'),
  {mediaObjectValidationSchema} = require('./media-object.validation-schema'),
  {mediaObjectReferenceValidationSchema} = require('./../reference-validation-schemas/media-object.reference-validation-schema'),
  {personReferenceValidationSchema} = require('./../reference-validation-schemas/person.reference-validation-schema'),
  {textValidationSchema} = require('./text.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),
  {musicGroupReferenceValidationSchema} = require('./../reference-validation-schemas/music-group.reference-validation-schema'),

  videoObjectValidationSchema = validationSchemaModel(
    'VideoObject',
    {
      actor: Joi.alternatives(personReferenceValidationSchema).allow(null),
      caption: Joi.alternatives(
        mediaObjectReferenceValidationSchema,
        textValidationSchema
      ),
      director: Joi.alternatives(
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema)
      ).allow(null),
      embeddedTextCaption: textValidationSchema,
      musicBy: Joi.alternatives(
        musicGroupReferenceValidationSchema,
        personReferenceValidationSchema
      ).allow(null),
      thumbnail: Joi.alternatives(imageObjectReferenceValidationSchema).allow(null),
      transcript: textValidationSchema,
      videoFrameSize: textValidationSchema,
      videoQuality: textValidationSchema,

      ...mediaObjectValidationSchema
    }
  );

module.exports = {
  videoObjectValidationSchema
};
