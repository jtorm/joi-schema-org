/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  genderTypeValidationSchema = Joi.string().valid(
    'https://schema.org/Female',
    'https://schema.org/Male',
    'Female', 'Male'
  ).allow(null);

module.exports = {
  genderTypeValidationSchema
};
