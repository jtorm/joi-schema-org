/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  actionStatusTypeValidationSchema = Joi.string().valid(
    'https://schema.org/ActiveActionStatus',
    'https://schema.org/CompletedActionStatus',
    'https://schema.org/FailedActionStatus',
    'https://schema.org/PotentialActionStatus',
    'ActiveActionStatus', 'CompletedActionStatus', 'FailedActionStatus', 'PotentialActionStatus'
  );

module.exports = {
  actionStatusTypeValidationSchema
};
