/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  imageObjectReferenceValidationSchema = referenceValidationSchemaModel('ImageObject');

module.exports = {
  imageObjectReferenceValidationSchema
};
