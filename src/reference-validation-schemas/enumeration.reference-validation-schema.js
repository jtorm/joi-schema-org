/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  enumerationReferenceValidationSchema = referenceValidationSchemaModel('Enumeration');

module.exports = {
  enumerationReferenceValidationSchema
};
