/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  dateValidationSchema = Joi.alternatives(Joi.date().iso()).allow(null);

module.exports = {
  dateValidationSchema
};
