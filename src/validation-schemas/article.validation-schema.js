/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  {creativeWorkValidationSchema} = require('./creative-work.validation-schema'),
  {creativeWorkReferenceValidationSchema} = require('./../reference-validation-schemas/creative-work.reference-validation-schema'),
  {integerValidationSchema} = require('./integer.validation-schema'),
  {textValidationSchema} = require('./text.validation-schema'),
  {speakableSpecificationReferenceValidationSchema} = require('./../reference-validation-schemas/speakable-specification.reference-validation-schema'),
  {urlValidationSchema} = require('./url.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),

  articleValidationSchema = validationSchemaModel(
    'Article',
    {
      articleBody: textValidationSchema,
      articleSection: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema)
      ),
      backstory: Joi.alternatives(
        creativeWorkReferenceValidationSchema,
        Joi.array().items(creativeWorkReferenceValidationSchema),
        textValidationSchema,
        Joi.array().items(textValidationSchema)
      ),
      pageEnd: Joi.alternatives(
        integerValidationSchema,
        textValidationSchema
      ),
      pageStart: Joi.alternatives(
        integerValidationSchema,
        textValidationSchema
      ),
      pagination: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema)
      ),
      speakable: Joi.alternatives(
        speakableSpecificationReferenceValidationSchema,
        Joi.array().items(speakableSpecificationReferenceValidationSchema),
        urlValidationSchema,
        Joi.array().items(urlValidationSchema)
      ),
      wordCount: integerValidationSchema,

      ...creativeWorkValidationSchema
    }
  );

module.exports = {
  articleValidationSchema
};
