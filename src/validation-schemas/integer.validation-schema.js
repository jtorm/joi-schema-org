/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  integerValidationSchema = Joi.number().integer().allow(null);

module.exports = {
  integerValidationSchema
};
