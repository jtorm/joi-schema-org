/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  {creativeWorkReferenceValidationSchema} = require('./../reference-validation-schemas/creative-work.reference-validation-schema'),
  {creativeWorkValidationSchema} = require('./creative-work.validation-schema'),
  {dataFeedReferenceValidationSchema} = require('./../reference-validation-schemas/data-feed.reference-validation-schema'),
  {imageObjectReferenceValidationSchema} = require('./../reference-validation-schemas/image-object.reference-validation-schema'),
  {softwareApplicationReferenceValidationSchema} = require('./../reference-validation-schemas/software-application.reference-validation-schema'),
  {textValidationSchema} = require('./text.validation-schema'),
  {urlValidationSchema} = require("./url.validation-schema"),
  {validationSchemaModel} = require('./../validation-schema.model'),

  softwareApplicationValidationSchema = validationSchemaModel(
    'SoftwareApplication',
    {
      applicationCategory: Joi.alternatives(
        textValidationSchema,
        urlValidationSchema
      ),
      applicationSubCategory: Joi.alternatives(
        textValidationSchema,
        urlValidationSchema
      ),
      applicationSuite: textValidationSchema,
      availableOnDevice: textValidationSchema,
      countriesNotSupported: textValidationSchema,
      countriesSupported: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema)
      ),
      downloadUrl: urlValidationSchema,
      featureList: Joi.alternatives(
        textValidationSchema,
        urlValidationSchema
      ),
      fileSize: textValidationSchema,
      installUrl: urlValidationSchema,
      memoryRequirements: Joi.alternatives(
        textValidationSchema,
        urlValidationSchema
      ),
      operatingSystem: textValidationSchema,
      permissions: textValidationSchema,
      processorRequirements: textValidationSchema,
      releaseNotes: Joi.alternatives(
        textValidationSchema,
        urlValidationSchema
      ),
      screenshot: Joi.alternatives(
        imageObjectReferenceValidationSchema,
        Joi.array().items(imageObjectReferenceValidationSchema)
      ),
      softwareAddOn: softwareApplicationReferenceValidationSchema,
      softwareHelp: creativeWorkReferenceValidationSchema,
      softwareRequirements: Joi.alternatives(
        textValidationSchema,
        urlValidationSchema
      ),
      softwareVersion: textValidationSchema,
      storageRequirements: Joi.alternatives(
        textValidationSchema,
        urlValidationSchema
      ),
      supportingData:	dataFeedReferenceValidationSchema,

      ...creativeWorkValidationSchema
    }
  );

module.exports = {
  softwareApplicationValidationSchema
};
