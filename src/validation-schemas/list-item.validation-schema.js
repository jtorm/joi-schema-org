/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  {anyThingReferenceValidationSchema} = require('./../reference-validation-schemas/any-thing.reference-validation-schema'),
  {integerValidationSchema} = require('./integer.validation-schema'),
  {listItemReferenceValidationSchema} = require('./../reference-validation-schemas/list-item.reference-validation-schema'),
  {textValidationSchema} = require('./text.validation-schema'),
  {thingValidationSchema} = require('./thing.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),

  listItemValidationSchema = validationSchemaModel(
    'ListItem',
    {
      item: anyThingReferenceValidationSchema,
      nextItem: Joi.alternatives(listItemReferenceValidationSchema).allow(null),
      position: Joi.alternatives(
        integerValidationSchema,
        textValidationSchema
      ),
      previousItem: Joi.alternatives(listItemReferenceValidationSchema).allow(null),

      ...thingValidationSchema
    }
  );

module.exports = {
  listItemValidationSchema
};
