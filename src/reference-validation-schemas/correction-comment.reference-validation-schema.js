/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  correctionCommentReferenceValidationSchema = referenceValidationSchemaModel('CorrectionComment');

module.exports = {
  correctionCommentReferenceValidationSchema
};
