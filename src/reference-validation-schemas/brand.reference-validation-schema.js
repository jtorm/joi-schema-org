/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  brandReferenceValidationSchema = referenceValidationSchemaModel('Brand');

module.exports = {
  brandReferenceValidationSchema
};
