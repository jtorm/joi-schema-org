/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  offerReferenceValidationSchema = referenceValidationSchemaModel('Offer');

module.exports = {
  offerReferenceValidationSchema
};
