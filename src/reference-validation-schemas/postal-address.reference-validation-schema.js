/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  postalAddressReferenceValidationSchema = referenceValidationSchemaModel('PostalAddress');

module.exports = {
  postalAddressReferenceValidationSchema
};
