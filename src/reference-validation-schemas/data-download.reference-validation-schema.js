/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  dataDownloadReferenceValidationSchema = referenceValidationSchemaModel('DataDownload');

module.exports = {
  dataDownloadReferenceValidationSchema
};
