/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {urlValidationSchema} = require('./url.validation-schema'),
  measurementTypeEnumerationValidationSchema = urlValidationSchema;

module.exports = {
  measurementTypeEnumerationValidationSchema
};
