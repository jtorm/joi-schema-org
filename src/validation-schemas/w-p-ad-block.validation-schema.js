/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {cssSelectorTypeValidationSchema} = require('./css-selector-type.validation-schema'),
  {creativeWorkValidationSchema} = require('./creative-work.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),
  {xPathTypeValidationSchema} = require('./x-path-type.validation-schema'),

  wPAdBlockValidationSchema = validationSchemaModel(
    'WPAdBlock',
    {
      cssSelector: cssSelectorTypeValidationSchema,
      xpath: xPathTypeValidationSchema,

      ...creativeWorkValidationSchema
    }
  );

module.exports = {
  wPAdBlockValidationSchema
};
