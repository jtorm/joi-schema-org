/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  numberValidationSchema = Joi.number().allow(null);

module.exports = {
  numberValidationSchema
};
