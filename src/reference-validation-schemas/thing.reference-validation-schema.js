/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  thingReferenceValidationSchema = referenceValidationSchemaModel('Thing');

module.exports = {
  thingReferenceValidationSchema
};
