/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  {creativeWorkValidationSchema} = require('./creative-work.validation-schema'),
  {dataDownloadReferenceValidationSchema} = require('./../reference-validation-schemas/data-download.reference-validation-schema'),
  {dataCatalogReferenceValidationSchema} = require('./../reference-validation-schemas/data-catalog.reference-validation-schema'),
  {propertyValueReferenceValidationSchema} = require('./../reference-validation-schemas/property-value.reference-validation-schema'),
  {textValidationSchema} = require('./text.validation-schema'),
  {urlValidationSchema} = require('./url.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),

  datasetValidationSchema = validationSchemaModel(
    'Dataset',
    {
      distribution: Joi.alternatives(dataDownloadReferenceValidationSchema).allow(null),
      includedInDataCatalog: dataCatalogReferenceValidationSchema,
      issn: textValidationSchema,
      measurementTechnique: Joi.alternatives(
        textValidationSchema,
        urlValidationSchema
      ),
      variableMeasured: Joi.alternatives(
        propertyValueReferenceValidationSchema,
        Joi.array().items(propertyValueReferenceValidationSchema)
      ),

      ...creativeWorkValidationSchema
    }
  );

module.exports = {
  datasetValidationSchema
};
