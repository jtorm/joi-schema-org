/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {actionValidationSchema} = require('./action.validation-schema'),
  {textValidationSchema} = require('./text.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),

  searchActionValidationSchema = validationSchemaModel(
    'SearchAction',
    {
      query: textValidationSchema,

      ...actionValidationSchema
    }
  );

module.exports = {
  searchActionValidationSchema
};
