/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  listItemReferenceValidationSchema = referenceValidationSchemaModel('ListItem');

module.exports = {
  listItemReferenceValidationSchema
};
