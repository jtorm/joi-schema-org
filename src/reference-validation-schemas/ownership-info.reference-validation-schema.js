/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  ownershipInfoReferenceValidationSchema = referenceValidationSchemaModel('OwnershipInfo');

module.exports = {
  ownershipInfoReferenceValidationSchema
};
