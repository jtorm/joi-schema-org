/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  dataFeedReferenceValidationSchema = referenceValidationSchemaModel('DataFeed');

module.exports = {
  dataFeedReferenceValidationSchema
};
