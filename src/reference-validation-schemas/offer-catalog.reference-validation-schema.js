/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  offerCatalogReferenceValidationSchema = referenceValidationSchemaModel('OfferCatalog');

module.exports = {
  offerCatalogReferenceValidationSchema
};
