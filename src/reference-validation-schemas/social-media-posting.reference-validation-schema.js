/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  socialMediaPostingReferenceValidationSchema = referenceValidationSchemaModel('SocialMediaPosting');

module.exports = {
  socialMediaPostingReferenceValidationSchema
};
