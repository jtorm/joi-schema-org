/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  videoObjectReferenceValidationSchema = referenceValidationSchemaModel('VideoObject');

module.exports = {
  videoObjectReferenceValidationSchema
};
