/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  grantReferenceValidationSchema = referenceValidationSchemaModel('Grant');

module.exports = {
  grantReferenceValidationSchema
};
