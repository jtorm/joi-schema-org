/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  urlValidationSchema = Joi.alternatives(new RegExp('^http(s)?://')).allow(null);

module.exports = {
  urlValidationSchema
};
