/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {mediaObjectValidationSchema} = require('./media-object.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),

  musicVideoObjectValidationSchema = validationSchemaModel(
    'MusicVideoObject',
    mediaObjectValidationSchema
  );

module.exports = {
  musicVideoObjectValidationSchema
};
