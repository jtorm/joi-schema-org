/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  actionReferenceValidationSchema = referenceValidationSchemaModel('Action|AchieveAction|AssessAction|ConsumeAction|ControlAction|CreateAction|FindAction|InteractAction|MoveAction|OrganizeAction|PlayAction|SearchAction|SeekToAction|SolveMathAction|TradeAction|TransferAction|UpdateAction');

module.exports = {
  actionReferenceValidationSchema
};
