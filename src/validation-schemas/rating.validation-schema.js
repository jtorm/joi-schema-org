/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  {numberValidationSchema} = require('./number.validation-schema'),
  {organizationReferenceValidationSchema} = require("./../reference-validation-schemas/organization.reference-validation-schema"),
  {personReferenceValidationSchema} = require("./../reference-validation-schemas/person.reference-validation-schema"),
  {textValidationSchema} = require('./text.validation-schema'),
  {thingValidationSchema} = require('./thing.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),

  ratingValidationSchema = validationSchemaModel(
    'Rating',
    {
      author: Joi.alternatives(
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema),
        organizationReferenceValidationSchema,
        Joi.array().items(organizationReferenceValidationSchema)
      ).allow(null),
      bestRating: Joi.alternatives(
        textValidationSchema,
        numberValidationSchema
      ).allow(null),
      ratingExplanation: textValidationSchema,
      ratingValue: Joi.alternatives(
        textValidationSchema,
        numberValidationSchema
      ),
      reviewAspect: textValidationSchema,
      worstRating: Joi.alternatives(
        textValidationSchema,
        numberValidationSchema
      ),

      ...thingValidationSchema
    }
  );

module.exports = {
  ratingValidationSchema
};
