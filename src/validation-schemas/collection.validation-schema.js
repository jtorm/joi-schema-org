/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {creativeWorkValidationSchema} = require('./creative-work.validation-schema'),
  {integerValidationSchema} = require('./integer.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),

  collectionValidationSchema = validationSchemaModel(
    'Collection',
    {
      collectionSize: integerValidationSchema,

      ...creativeWorkValidationSchema
    }
  );

module.exports = {
  collectionValidationSchema
};
