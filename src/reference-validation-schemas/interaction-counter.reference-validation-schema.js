/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  interactionCounterReferenceValidationSchema = referenceValidationSchemaModel('InteractionCounter');

module.exports = {
  interactionCounterReferenceValidationSchema
};
