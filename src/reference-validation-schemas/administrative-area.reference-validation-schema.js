/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  administrativeAreaReferenceValidationSchema = referenceValidationSchemaModel('AdministrativeArea');

module.exports = {
  administrativeAreaReferenceValidationSchema
};
