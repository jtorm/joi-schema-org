/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  specialtyReferenceValidationSchema = referenceValidationSchemaModel('Specialty');

module.exports = {
  specialtyReferenceValidationSchema
};
