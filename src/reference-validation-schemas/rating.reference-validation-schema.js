/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  ratingReferenceValidationSchema = referenceValidationSchemaModel('Rating');

module.exports = {
  ratingReferenceValidationSchema
};
