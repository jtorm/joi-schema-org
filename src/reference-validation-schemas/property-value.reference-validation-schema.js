/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  propertyValueReferenceValidationSchema = {
    "@context": Joi.string().pattern(/^https:\/\/schema.org\/$/),
    "@type": Joi.string().pattern(/^PropertyValue$/).required(),
    identifier: Joi.string().required()
  };

module.exports = {
  propertyValueReferenceValidationSchema
};
