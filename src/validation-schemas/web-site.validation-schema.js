/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {creativeWorkValidationSchema} = require('./creative-work.validation-schema'),
  {textValidationSchema} = require('./text.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),

  webSiteValidationSchema = validationSchemaModel(
    'WebSite',
    {
      issn: textValidationSchema,

      ...creativeWorkValidationSchema
    }
  );

module.exports = {
  webSiteValidationSchema
};
