/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  {anyThingReferenceValidationSchema} = require("./../reference-validation-schemas/any-thing.reference-validation-schema"),
  {datasetValidationSchema} = require('./dataset.validation-schema'),
  {dataFeedItemReferenceValidationSchema} = require('./../reference-validation-schemas/data-feed-item.reference-validation-schema'),
  {textValidationSchema} = require('./text.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),

  dataFeedValidationSchema = validationSchemaModel(
    'DataFeed',
    {
      dataFeedElement: Joi.alternatives(
        dataFeedItemReferenceValidationSchema,
        textValidationSchema,
        anyThingReferenceValidationSchema
      ),

      ...datasetValidationSchema
    }
  );

module.exports = {
  dataFeedValidationSchema
};
