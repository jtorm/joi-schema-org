/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {anyThingReferenceValidationSchema} = require('./../reference-validation-schemas/any-thing.reference-validation-schema'),
  {integerValidationSchema} = require('./integer.validation-schema'),
  {ratingValidationSchema} = require('./rating.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),

  aggregateRatingValidationSchema = validationSchemaModel(
    'AggregateRating',
    {
      itemReviewed: anyThingReferenceValidationSchema,
      ratingCount: integerValidationSchema,
      reviewCount: integerValidationSchema,

      ...ratingValidationSchema
    }
  );

module.exports = {
  aggregateRatingValidationSchema
};
