/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  publicationEventReferenceValidationSchema = referenceValidationSchemaModel('PublicationEvent');

module.exports = {
  publicationEventReferenceValidationSchema
};
