/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  digitalPlatformEnumerationValidationSchema = Joi.string().valid(
    'https://schema.org/AndroidPlatform',
    'https://schema.org/DesktopWebPlatform',
    'https://schema.org/GenericWebPlatform',
    'https://schema.org/IOSPlatform',
    'https://schema.org/MobileWebPlatform',
    'AndroidPlatform', 'DesktopWebPlatform', 'GenericWebPlatform', 'IOSPlatform', 'MobileWebPlatform'
  ).allow(null);

module.exports = {
  digitalPlatformEnumerationValidationSchema
};
