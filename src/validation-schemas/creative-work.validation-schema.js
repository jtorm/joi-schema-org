/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),
  {validationSchemaModel} = require('./../validation-schema.model'),

  {alignmentObjectReferenceValidationSchema} = require('./../reference-validation-schemas/alignment-object.reference-validation-schema'),
  {anyThingReferenceValidationSchema} = require('./../reference-validation-schemas/any-thing.reference-validation-schema'),
  {audioObjectReferenceValidationSchema} = require('./../reference-validation-schemas/audio-object.reference-validation-schema'),
  {audienceReferenceValidationSchema} = require('./../reference-validation-schemas/audience.reference-validation-schema'),
  {aggregateRatingReferenceValidationSchema} = require('./../reference-validation-schemas/aggregate-rating.reference-validation-schema'),
  {booleanValidationSchema} = require('./../validation-schemas/boolean.validation-schema'),
  {claimReferenceValidationSchema} = require('./../reference-validation-schemas/claim.reference-validation-schema'),
  {clipReferenceValidationSchema} = require('./../reference-validation-schemas/clip.reference-validation-schema'),
  {commentReferenceValidationSchema} = require('./../reference-validation-schemas/comment.reference-validation-schema'),
  {countryReferenceValidationSchema} = require('./../reference-validation-schemas/country.reference-validation-schema'),
  {correctionCommentReferenceValidationSchema} = require('./../reference-validation-schemas/correction-comment.reference-validation-schema'),
  {creativeWorkReferenceValidationSchema} = require('./../reference-validation-schemas/creative-work.reference-validation-schema'),
  {dateValidationSchema} = require('./date.validation-schema'),
  {dateTimeValidationSchema} = require('./date-time.validation-schema'),
  {definedTermReferenceValidationSchema} = require('./../reference-validation-schemas/defined-term.reference-validation-schema'),
  {demandReferenceValidationSchema} = require('./../reference-validation-schemas/demand.reference-validation-schema'),
  {durationReferenceValidationSchema} = require('./../reference-validation-schemas/duration.reference-validation-schema'),
  {eventReferenceValidationSchema} = require('./../reference-validation-schemas/event.reference-validation-schema'),
  {grantReferenceValidationSchema} = require('./../reference-validation-schemas/grant.reference-validation-schema'),
  {integerValidationSchema} = require('./integer.validation-schema'),
  {interactionCounterReferenceValidationSchema} = require('./../reference-validation-schemas/interaction-counter.reference-validation-schema'),
  {itemListReferenceValidationSchema} = require('./../reference-validation-schemas/item-list.reference-validation-schema'),
  {languageReferenceValidationSchema} = require('./../reference-validation-schemas/language.reference-validation-schema'),
  {mediaObjectReferenceValidationSchema} = require('./../reference-validation-schemas/media-object.reference-validation-schema'),
  {musicRecordingReferenceValidationSchema} = require('./../reference-validation-schemas/music-recording.reference-validation-schema'),
  {numberValidationSchema} = require('./number.validation-schema'),
  {offerReferenceValidationSchema} = require('./../reference-validation-schemas/offer.reference-validation-schema'),
  {organizationReferenceValidationSchema} = require('./../reference-validation-schemas/organization.reference-validation-schema'),
  {personReferenceValidationSchema} = require('./../reference-validation-schemas/person.reference-validation-schema'),
  {placeReferenceValidationSchema} = require('./../reference-validation-schemas/place.reference-validation-schema'),
  {productReferenceValidationSchema} = require('./../reference-validation-schemas/product.reference-validation-schema'),
  {publicationEventReferenceValidationSchema} = require('./../reference-validation-schemas/publication-event.reference-validation-schema'),
  {quantitativeValueReferenceValidationSchema} = require('./../reference-validation-schemas/quantitative-value.reference-validation-schema'),
  {ratingValidationSchema} = require('./rating.validation-schema'),
  {reviewReferenceValidationSchema} = require('./../reference-validation-schemas/review.reference-validation-schema'),
  {sizeSpecificationReferenceValidationSchema} = require('./../reference-validation-schemas/size-specification.reference-validation-schema'),
  {textValidationSchema} = require('./text.validation-schema'),
  {thingValidationSchema} = require('./thing.validation-schema'),
  {thingReferenceValidationSchema} = require('./../reference-validation-schemas/thing.reference-validation-schema'),
  {urlValidationSchema} = require('./url.validation-schema'),
  {videoObjectReferenceValidationSchema} = require('./../reference-validation-schemas/video-object.reference-validation-schema'),
  {webPageReferenceValidationSchema} = require('./../reference-validation-schemas/web-page.reference-validation-schema'),

  creativeWorkValidationSchema = validationSchemaModel(
    'CreativeWork',
    {
      about: Joi.alternatives(
        thingReferenceValidationSchema,
        Joi.array().items(thingReferenceValidationSchema)
      ).allow(null),
      abstract: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema)
      ),
      accessMode: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema)
      ),
      accessModeSufficient: Joi.alternatives(
        itemListReferenceValidationSchema,
        Joi.array().items(itemListReferenceValidationSchema)
      ).allow(null),
      accessibilityAPI: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema)
      ),
      accessibilityControl: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema)
      ),
      accessibilityFeature: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema)
      ),
      accessibilityHazard: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema)
      ),
      accessibilitySummary: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema)
      ),
      accountablePerson: Joi.alternatives(
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema)
      ).allow(null),
      acquireLicensePage: Joi.alternatives(
        urlValidationSchema,
        Joi.array().items(urlValidationSchema),
        creativeWorkReferenceValidationSchema,
        Joi.array().items(creativeWorkReferenceValidationSchema)
      ),
      aggregateRating: Joi.alternatives(
        aggregateRatingReferenceValidationSchema,
        Joi.array().items(aggregateRatingReferenceValidationSchema)
      ).allow(null),
      alternativeHeadline: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema)
      ),
      archivedAt: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema),
        webPageReferenceValidationSchema,
        Joi.array().items(webPageReferenceValidationSchema)
      ),
      assesses: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema),
        definedTermReferenceValidationSchema,
        Joi.array().items(definedTermReferenceValidationSchema)
      ),
      associatedMedia: Joi.alternatives(
        mediaObjectReferenceValidationSchema,
        Joi.array().items(mediaObjectReferenceValidationSchema)
      ).allow(null),
      audience: Joi.alternatives(
        audienceReferenceValidationSchema,
        Joi.array().items(audienceReferenceValidationSchema)
      ).allow(null),
      audio: Joi.alternatives(
        audioObjectReferenceValidationSchema,
        Joi.array().items(audioObjectReferenceValidationSchema),
        clipReferenceValidationSchema,
        Joi.array().items(clipReferenceValidationSchema),
        musicRecordingReferenceValidationSchema,
        Joi.array().items(musicRecordingReferenceValidationSchema)
      ).allow(null),
      author: Joi.alternatives(
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema),
        organizationReferenceValidationSchema,
        Joi.array().items(organizationReferenceValidationSchema)
      ).allow(null),
      award: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema)
      ),
      character: Joi.alternatives(
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema)
      ).allow(null),
      citation: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema),
        creativeWorkReferenceValidationSchema,
        Joi.array().items(creativeWorkReferenceValidationSchema)
      ),
      comment: Joi.alternatives(
        commentReferenceValidationSchema,
        Joi.array().items(commentReferenceValidationSchema)
      ).allow(null),
      commentCount: integerValidationSchema,
      conditionsOfAccess: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema)
      ),
      contentLocation: Joi.alternatives(
        placeReferenceValidationSchema,
        Joi.array().items(placeReferenceValidationSchema)
      ).allow(null),
      contentRating: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema),
        ratingValidationSchema,
        Joi.array().items(ratingValidationSchema)
      ),
      contentReferenceTime: dateTimeValidationSchema,
      contributor: Joi.alternatives(
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema),
        organizationReferenceValidationSchema,
        Joi.array().items(organizationReferenceValidationSchema)
      ).allow(null),
      copyrightHolder: Joi.alternatives(
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema),
        organizationReferenceValidationSchema,
        Joi.array().items(organizationReferenceValidationSchema)
      ).allow(null),
      copyrightNotice: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema)
      ),
      copyrightYear: numberValidationSchema.min(1900).max(new Date().getFullYear()),
      correction: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema),
        correctionCommentReferenceValidationSchema,
        Joi.array().items(correctionCommentReferenceValidationSchema)
      ),
      countryOfOrigin: Joi.alternatives(
        countryReferenceValidationSchema,
        Joi.array().items(countryReferenceValidationSchema)
      ).allow(null),
      creativeWorkStatus: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema),
        definedTermReferenceValidationSchema,
        Joi.array().items(definedTermReferenceValidationSchema)
      ),
      creator: Joi.alternatives(
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema),
        organizationReferenceValidationSchema,
        Joi.array().items(organizationReferenceValidationSchema)
      ).allow(null),
      creditText: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema)
      ),
      dateCreated: Joi.alternatives(
        dateValidationSchema,
        Joi.array().items(dateValidationSchema),
        dateTimeValidationSchema,
        Joi.array().items(dateTimeValidationSchema)
      ),
      dateModified: Joi.alternatives(
        dateValidationSchema,
        Joi.array().items(dateValidationSchema),
        dateTimeValidationSchema,
        Joi.array().items(dateTimeValidationSchema)
      ),
      datePublished: Joi.alternatives(
        dateValidationSchema,
        Joi.array().items(dateValidationSchema),
        dateTimeValidationSchema,
        Joi.array().items(dateTimeValidationSchema)
      ),
      discussionUrl: urlValidationSchema,
      editEIDR: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema)
      ),
      editor: Joi.alternatives(
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema)
      ).allow(null),
      educationalAlignment: Joi.alternatives(
        alignmentObjectReferenceValidationSchema,
        Joi.array().items(alignmentObjectReferenceValidationSchema)
      ).allow(null),
      educationalLevel: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema),
        definedTermReferenceValidationSchema,
        Joi.array().items(definedTermReferenceValidationSchema)
      ),
      educationalUse: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema),
        definedTermReferenceValidationSchema,
        Joi.array().items(definedTermReferenceValidationSchema)
      ),
      encoding: Joi.alternatives(
        mediaObjectReferenceValidationSchema,
        Joi.array().items(mediaObjectReferenceValidationSchema)
      ).allow(null),
      encodingFormat: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema)
      ),
      exampleOfWork: Joi.alternatives(
        anyThingReferenceValidationSchema,
        Joi.array().items(anyThingReferenceValidationSchema)
      ).allow(null),
      expires: Joi.alternatives(
        dateValidationSchema,
        Joi.array().items(dateValidationSchema),
        dateTimeValidationSchema,
        Joi.array().items(dateTimeValidationSchema)
      ),
      funder: Joi.alternatives(
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema),
        organizationReferenceValidationSchema,
        Joi.array().items(organizationReferenceValidationSchema)
      ).allow(null),
      funding: Joi.alternatives(
        grantReferenceValidationSchema,
        Joi.array().items(grantReferenceValidationSchema)
      ).allow(null),
      genre: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema)
      ),
      hasPart: Joi.alternatives(
        anyThingReferenceValidationSchema,
        Joi.array().items(anyThingReferenceValidationSchema)
      ).allow(null),
      headline: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema)
      ),
      inLanguage: Joi.alternatives(
        textValidationSchema,
        languageReferenceValidationSchema
      ),
      interactionStatistic: Joi.alternatives(
        interactionCounterReferenceValidationSchema,
        Joi.array().items(interactionCounterReferenceValidationSchema)
      ).allow(null),
      interactivityType: textValidationSchema.pattern(/^(active|expositive|mixed)$/),
      interpretedAsClaim: Joi.alternatives(
        claimReferenceValidationSchema,
        Joi.array().items(claimReferenceValidationSchema)
      ).allow(null),
      isAccessibleForFree: booleanValidationSchema,
      isBasedOn: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema),
        creativeWorkReferenceValidationSchema,
        Joi.array().items(creativeWorkReferenceValidationSchema),
        productReferenceValidationSchema,
        Joi.array().items(productReferenceValidationSchema)
      ),
      isFamilyFriendly: Joi.bool(),
      isPartOf: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema),
        anyThingReferenceValidationSchema,
        Joi.array().items(anyThingReferenceValidationSchema)
      ),
      keywords: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema),
        definedTermReferenceValidationSchema,
        Joi.array().items(definedTermReferenceValidationSchema)
      ),
      learningResourceType: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema),
        definedTermReferenceValidationSchema,
        Joi.array().items(definedTermReferenceValidationSchema)
      ),
      license: Joi.alternatives(
        urlValidationSchema,
        Joi.array().items(urlValidationSchema),
        creativeWorkReferenceValidationSchema,
        Joi.array().items(creativeWorkReferenceValidationSchema)
      ),
      locationCreated: Joi.alternatives(
        placeReferenceValidationSchema,
        Joi.array().items(placeReferenceValidationSchema)
      ).allow(null),
      mainEntity: Joi.alternatives(anyThingReferenceValidationSchema).allow(null),
      maintainer: Joi.alternatives(
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema),
        organizationReferenceValidationSchema,
        Joi.array().items(organizationReferenceValidationSchema)
      ).allow(null),
      material: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema),
        productReferenceValidationSchema,
        Joi.array().items(productReferenceValidationSchema)
      ),
      materialExtent: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema),
        quantitativeValueReferenceValidationSchema,
        Joi.array().items(quantitativeValueReferenceValidationSchema)
      ),
      mentions: Joi.alternatives(
        anyThingReferenceValidationSchema,
        Joi.array().items(anyThingReferenceValidationSchema)
      ).allow(null),
      offers: Joi.alternatives(
        demandReferenceValidationSchema,
        Joi.array().items(demandReferenceValidationSchema),
        offerReferenceValidationSchema,
        Joi.array().items(offerReferenceValidationSchema)
      ).allow(null),
      pattern: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema),
        definedTermReferenceValidationSchema,
        Joi.array().items(definedTermReferenceValidationSchema)
      ),
      position: Joi.alternatives(
        textValidationSchema,
        integerValidationSchema
      ),
      producer: Joi.alternatives(
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema),
        organizationReferenceValidationSchema,
        Joi.array().items(organizationReferenceValidationSchema)
      ).allow(null),
      provider: Joi.alternatives(
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema),
        organizationReferenceValidationSchema,
        Joi.array().items(organizationReferenceValidationSchema)
      ).allow(null),
      publication: Joi.alternatives(
        publicationEventReferenceValidationSchema,
        Joi.array().items(publicationEventReferenceValidationSchema)
      ).allow(null),
      publisher: Joi.alternatives(
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema),
        organizationReferenceValidationSchema,
        Joi.array().items(organizationReferenceValidationSchema)
      ).allow(null),
      publisherImprint: Joi.alternatives(
        organizationReferenceValidationSchema,
        Joi.array().items(organizationReferenceValidationSchema)
      ).allow(null),
      publishingPrinciples: Joi.alternatives(
        urlValidationSchema,
        Joi.array().items(urlValidationSchema),
        creativeWorkReferenceValidationSchema,
        Joi.array().items(creativeWorkReferenceValidationSchema)
      ),
      recordedAt: Joi.alternatives(
        eventReferenceValidationSchema,
        Joi.array().items(eventReferenceValidationSchema)
      ).allow(null),
      releasedEvent: Joi.alternatives(
        publicationEventReferenceValidationSchema,
        Joi.array().items(publicationEventReferenceValidationSchema)
      ).allow(null),
      review: Joi.alternatives(
        reviewReferenceValidationSchema,
        Joi.array().items(reviewReferenceValidationSchema)
      ).allow(null),
      schemaVersion: textValidationSchema,
      sdDatePublished: dateValidationSchema,
      sdLicense: Joi.alternatives(
        urlValidationSchema,
        Joi.array().items(urlValidationSchema),
        creativeWorkReferenceValidationSchema,
        Joi.array().items(creativeWorkReferenceValidationSchema)
      ).allow(null),
      sdPublisher: Joi.alternatives(
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema),
        organizationReferenceValidationSchema,
        Joi.array().items(organizationReferenceValidationSchema)
      ).allow(null),
      size: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema),
        definedTermReferenceValidationSchema,
        Joi.array().items(definedTermReferenceValidationSchema),
        quantitativeValueReferenceValidationSchema,
        Joi.array().items(quantitativeValueReferenceValidationSchema),
        sizeSpecificationReferenceValidationSchema,
        Joi.array().items(sizeSpecificationReferenceValidationSchema)
      ),
      sourceOrganization: Joi.alternatives(
        organizationReferenceValidationSchema,
        Joi.array().items(organizationReferenceValidationSchema)
      ).allow(null),
      spatial: Joi.alternatives(
        placeReferenceValidationSchema,
        Joi.array().items(placeReferenceValidationSchema)
      ).allow(null),
      spatialCoverage: Joi.alternatives(
        placeReferenceValidationSchema,
        Joi.array().items(placeReferenceValidationSchema)
      ).allow(null),
      sponsor: Joi.alternatives(
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema),
        organizationReferenceValidationSchema,
        Joi.array().items(organizationReferenceValidationSchema)
      ).allow(null),
      teaches: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema),
        definedTermReferenceValidationSchema,
        Joi.array().items(definedTermReferenceValidationSchema)
      ),
      temporal: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema),
        dateTimeValidationSchema,
        Joi.array().items(dateTimeValidationSchema)
      ),
      temporalCoverage: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema),
        dateTimeValidationSchema,
        Joi.array().items(dateTimeValidationSchema)
      ),
      text: textValidationSchema,
      thumbnailUrl: Joi.alternatives(
        urlValidationSchema,
        Joi.array().items(urlValidationSchema)
      ),
      timeRequired: Joi.alternatives(durationReferenceValidationSchema).allow(null),
      translator: Joi.alternatives(
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema),
        organizationReferenceValidationSchema,
        Joi.array().items(organizationReferenceValidationSchema)
      ).allow(null),
      typicalAgeRange: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema)
      ),
      usageInfo: Joi.alternatives(
        urlValidationSchema,
        Joi.array().items(urlValidationSchema),
        creativeWorkReferenceValidationSchema,
        Joi.array().items(creativeWorkReferenceValidationSchema)
      ),
      version: Joi.alternatives(
        textValidationSchema,
        numberValidationSchema
      ),
      video: Joi.alternatives(
        clipReferenceValidationSchema,
        Joi.array().items(clipReferenceValidationSchema),
        videoObjectReferenceValidationSchema,
        Joi.array().items(videoObjectReferenceValidationSchema)
      ).allow(null),
      workExample: Joi.alternatives(
        anyThingReferenceValidationSchema,
        Joi.array().items(anyThingReferenceValidationSchema)
      ).allow(null),
      workTranslation: Joi.alternatives(
        anyThingReferenceValidationSchema,
        Joi.array().items(anyThingReferenceValidationSchema)
      ).allow(null),

      ...thingValidationSchema
    }
  );

module.exports = {
  creativeWorkValidationSchema
};
