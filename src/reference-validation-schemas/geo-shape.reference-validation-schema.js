/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  geoShapeReferenceValidationSchema = referenceValidationSchemaModel('GeoShape');

module.exports = {
  geoShapeReferenceValidationSchema
};
