/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  {mediaObjectValidationSchema} = require('./media-object.validation-schema'),
  {textValidationSchema} = require('./text.validation-schema'),
  {urlValidationSchema} = require('./url.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),

  dataDownloadValidationSchema = validationSchemaModel(
    'DataDownload',
    {
      measurementTechnique: Joi.alternatives(
        textValidationSchema,
        urlValidationSchema
      ),

      ...mediaObjectValidationSchema
    }
  );

module.exports = {
  dataDownloadValidationSchema
};
