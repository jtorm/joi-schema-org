/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  {anyThingReferenceValidationSchema} = require("./../reference-validation-schemas/any-thing.reference-validation-schema"),
  {creativeWorkValidationSchema} = require('./creative-work.validation-schema'),
  {organizationReferenceValidationSchema} = require("./../reference-validation-schemas/organization.reference-validation-schema"),
  {personReferenceValidationSchema} = require("./../reference-validation-schemas/person.reference-validation-schema"),
  {validationSchemaModel} = require('./../validation-schema.model'),

  claimValidationSchema = validationSchemaModel(
    'Claim',
    {
      appearance: anyThingReferenceValidationSchema,
      claimInterpreter: Joi.alternatives(
        personReferenceValidationSchema,
        organizationReferenceValidationSchema,
      ),
      firstAppearance: Joi.alternatives(anyThingReferenceValidationSchema).allow(null),

      ...creativeWorkValidationSchema
    }
  );

module.exports = {
  claimValidationSchema
};
