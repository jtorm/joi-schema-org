/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  webPageReferenceValidationSchema = referenceValidationSchemaModel('WebPage');

module.exports = {
  webPageReferenceValidationSchema
};
