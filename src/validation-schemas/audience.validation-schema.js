/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  {administrativeAreaReferenceValidationSchema} = require('./../reference-validation-schemas/administrative-area.reference-validation-schema'),
  {textValidationSchema} = require('./text.validation-schema'),
  {thingValidationSchema} = require('./thing.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),

  audienceValidationSchema = validationSchemaModel(
    'Audience',
    {
      audienceType: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema)
      ),
      geographicArea: Joi.alternatives(
        administrativeAreaReferenceValidationSchema,
        Joi.array().items(administrativeAreaReferenceValidationSchema)
      ).allow(null),

      ...thingValidationSchema
    }
  );

module.exports = {
  audienceValidationSchema
};
