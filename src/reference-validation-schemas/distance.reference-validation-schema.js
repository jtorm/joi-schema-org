/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  distanceReferenceValidationSchema = referenceValidationSchemaModel('Distance');

module.exports = {
  distanceReferenceValidationSchema
};
