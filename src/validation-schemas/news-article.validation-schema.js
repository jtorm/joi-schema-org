/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {articleValidationSchema} = require('./article.validation-schema'),
  {textValidationSchema} = require('./text.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),

  newsArticleValidationSchema = validationSchemaModel(
    'NewsArticle',
    {
      dateline: textValidationSchema,
      printColumn: textValidationSchema,
      printEdition: textValidationSchema,
      printPage: textValidationSchema,
      printSection: textValidationSchema,

      ...articleValidationSchema
    }
  );

module.exports = {
  newsArticleValidationSchema
};
