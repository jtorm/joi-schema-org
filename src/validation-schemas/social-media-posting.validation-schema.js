/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  {articleValidationSchema} = require('./article.validation-schema'),
  {anyThingReferenceValidationSchema} = require('./../reference-validation-schemas/any-thing.reference-validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),

  socialMediaPostingValidationSchema = validationSchemaModel(
    'SocialMediaPosting',
    {
      sharedContent: Joi.alternatives(
        anyThingReferenceValidationSchema,
        Joi.array().items(anyThingReferenceValidationSchema)
      ).allow(null),

      ...articleValidationSchema
    }
  );

module.exports = {
  socialMediaPostingValidationSchema
};
