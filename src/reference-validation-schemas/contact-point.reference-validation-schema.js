/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  contactPointReferenceValidationSchema = referenceValidationSchemaModel('ContactPoint');

module.exports = {
  contactPointReferenceValidationSchema
};
