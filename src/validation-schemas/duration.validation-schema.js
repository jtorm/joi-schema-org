/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {thingValidationSchema} = require('./thing.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),

  durationValidationSchema = validationSchemaModel(
    'Duration',
    thingValidationSchema
  );

module.exports = {
  durationValidationSchema
};
