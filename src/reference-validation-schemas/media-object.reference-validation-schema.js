/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  mediaObjectReferenceValidationSchema = referenceValidationSchemaModel('MediaObject');

module.exports = {
  mediaObjectReferenceValidationSchema
};
