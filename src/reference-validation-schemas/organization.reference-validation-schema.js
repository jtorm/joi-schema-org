/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  organizationReferenceValidationSchema = referenceValidationSchemaModel('Organization');

module.exports = {
  organizationReferenceValidationSchema
};
