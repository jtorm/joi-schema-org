/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  quantitativeValueReferenceValidationSchema = referenceValidationSchemaModel('QuantitativeValue');

module.exports = {
  quantitativeValueReferenceValidationSchema
};
