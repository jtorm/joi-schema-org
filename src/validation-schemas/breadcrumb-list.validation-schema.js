/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {itemListValidationSchema} = require('./item-list.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),

  breadcrumbListValidationSchema = validationSchemaModel(
    'BreadcrumbList',
    itemListValidationSchema
  );

module.exports = {
  breadcrumbListValidationSchema
};
