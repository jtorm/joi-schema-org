/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  textValidationSchema = Joi.string().allow(null);

module.exports = {
  textValidationSchema
};
