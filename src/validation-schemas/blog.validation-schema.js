/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {blogPostingReferenceValidationSchema} = require('./../reference-validation-schemas/blog-posting.reference-validation-schema'),
  {creativeWorkValidationSchema} = require('./creative-work.validation-schema'),
  {textValidationSchema} = require('./text.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),

  blogValidationSchema = validationSchemaModel(
    'Blog',
    {
      blogPost: blogPostingReferenceValidationSchema,
      issn: textValidationSchema,

      ...creativeWorkValidationSchema
    }
  );

module.exports = {
  blogValidationSchema
};
