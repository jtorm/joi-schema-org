/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  mediaSubscriptionReferenceValidationSchema = referenceValidationSchemaModel('MediaSubscription');

module.exports = {
  mediaSubscriptionReferenceValidationSchema
};
