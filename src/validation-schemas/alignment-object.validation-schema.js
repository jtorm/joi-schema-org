/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {textValidationSchema} = require('./text.validation-schema'),
  {thingValidationSchema} = require('./thing.validation-schema'),
  {urlValidationSchema} = require('./url.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),

  alignmentObjectValidationSchema = validationSchemaModel(
    'AlignmentObject',
    {
      alignmentType: textValidationSchema,
      educationalFramework: textValidationSchema,
      targetDescription: textValidationSchema,
      targetName: textValidationSchema,
      targetUrl: urlValidationSchema,

      ...thingValidationSchema
    }
  );

module.exports = {
  alignmentObjectValidationSchema
};
