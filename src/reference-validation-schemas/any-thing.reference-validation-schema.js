/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  {languageReferenceValidationSchema} = require("./language.reference-validation-schema"),
  {propertyValueReferenceValidationSchema} = require('./property-value.reference-validation-schema'),
  {textValidationSchema} = require("./../validation-schemas/text.validation-schema"),
  {urlValidationSchema} = require('./../validation-schemas/url.validation-schema'),

  anyThingReferenceValidationSchema = {
    "@context": urlValidationSchema,
    "@type": Joi.string().required(),
    identifier: Joi.alternatives(
      Joi.string(),
      propertyValueReferenceValidationSchema,
    ).required(),
    inLanguage: Joi.alternatives(
      textValidationSchema,
      languageReferenceValidationSchema
    )
  };

module.exports = {
  anyThingReferenceValidationSchema
};
