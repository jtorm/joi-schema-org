/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  virtualLocationReferenceValidationSchema = referenceValidationSchemaModel('VirtualLocation');

module.exports = {
  virtualLocationReferenceValidationSchema
};
