/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  dataFeedItemReferenceValidationSchema = referenceValidationSchemaModel('DataFeedItem');

module.exports = {
  dataFeedItemReferenceValidationSchema
};
