/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  datasetReferenceValidationSchema = referenceValidationSchemaModel('Dataset');

module.exports = {
  datasetReferenceValidationSchema
};
