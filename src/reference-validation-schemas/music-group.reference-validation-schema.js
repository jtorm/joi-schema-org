/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  musicGroupReferenceValidationSchema = referenceValidationSchemaModel('MusicGroup');

module.exports = {
  musicGroupReferenceValidationSchema
};
