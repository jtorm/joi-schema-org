/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  booleanValidationSchema = Joi.alternatives(
      Joi.boolean(),
      Joi.string().valid(
      'True',
      'False',
      'https://schema.org/False',
      'https://schema.org/True'
    )
  ).allow(null);

module.exports = {
  booleanValidationSchema
};
