/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  {integerValidationSchema} = require('./integer.validation-schema'),
  {itemListOrderTypeValidationSchema} = require('./item-list-order-type.validation-schema'),
  {listItemReferenceValidationSchema} = require('./../reference-validation-schemas/list-item.reference-validation-schema'),
  {textValidationSchema} = require('./text.validation-schema'),
  {thingReferenceValidationSchema} = require('./../reference-validation-schemas/thing.reference-validation-schema'),
  {thingValidationSchema} = require('./thing.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),

  itemListValidationSchema = validationSchemaModel(
    'ItemList',
    {
      itemListElement: Joi.alternatives(
        listItemReferenceValidationSchema,
        Joi.array().items(listItemReferenceValidationSchema),
        textValidationSchema,
        Joi.array().items(textValidationSchema),
        thingReferenceValidationSchema,
        Joi.array().items(thingReferenceValidationSchema)
      ),
      itemListOrder: Joi.alternatives(
        itemListOrderTypeValidationSchema
      ).allow(null),
      numberOfItems: integerValidationSchema,

      ...thingValidationSchema
    }
  );

module.exports = {
  itemListValidationSchema
};
