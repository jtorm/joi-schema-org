/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  {anyThingReferenceValidationSchema} = require('./../reference-validation-schemas/any-thing.reference-validation-schema'),
  {creativeWorkValidationSchema} = require('./creative-work.validation-schema'),
  {itemListReferenceValidationSchema} = require('./../reference-validation-schemas/item-list.reference-validation-schema'),
  {listItemReferenceValidationSchema} = require('./../reference-validation-schemas/list-item.reference-validation-schema'),
  {ratingReferenceValidationSchema} = require('./../reference-validation-schemas/rating.reference-validation-schema'),
  {reviewReferenceValidationSchema} = require('./../reference-validation-schemas/review.reference-validation-schema'),
  {textValidationSchema} = require('./text.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),

  reviewValidationSchema = validationSchemaModel(
    'Review',
    {
      associatedClaimReview: reviewReferenceValidationSchema,
      associatedMediaReview: reviewReferenceValidationSchema,
      associatedReview: reviewReferenceValidationSchema,
      itemReviewed: anyThingReferenceValidationSchema,
      negativeNotes: Joi.alternatives(
        itemListReferenceValidationSchema,
        listItemReferenceValidationSchema,
        textValidationSchema
      ),
      positiveNotes: Joi.alternatives(
        itemListReferenceValidationSchema,
        listItemReferenceValidationSchema,
        textValidationSchema
      ),
      reviewAspect: textValidationSchema,
      reviewBody: textValidationSchema,
      reviewRating: ratingReferenceValidationSchema,

      ...creativeWorkValidationSchema
    }
  );

module.exports = {
  reviewValidationSchema
};
