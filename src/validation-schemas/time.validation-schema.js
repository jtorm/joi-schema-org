/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  timeValidationSchema = Joi.string().regex(/^(\d{2})\:(\d{2})\:(\d{2})/).allow(null);

module.exports = {
  timeValidationSchema
};
