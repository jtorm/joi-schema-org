/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  claimReferenceValidationSchema = referenceValidationSchemaModel('Claim');

module.exports = {
  claimReferenceValidationSchema
};
