/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  itemListOrderTypeValidationSchema = Joi.string().valid(
    'https://schema.org/ItemListOrderAscending',
    'https://schema.org/ItemListOrderDescending',
    'https://schema.org/ItemListUnordered',
    'Ascending', 'Descending', 'Unordered'
  );

module.exports = {
  itemListOrderTypeValidationSchema
};
