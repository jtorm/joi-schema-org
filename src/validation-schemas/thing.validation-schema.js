/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  {actionReferenceValidationSchema} = require('./../reference-validation-schemas/action.reference-validation-schema'),
  {creativeWorkReferenceValidationSchema} = require('./../reference-validation-schemas/creative-work.reference-validation-schema'),
  {eventReferenceValidationSchema} = require('./../reference-validation-schemas/event.reference-validation-schema'),
  {imageObjectReferenceValidationSchema} = require('./../reference-validation-schemas/image-object.reference-validation-schema'),
  {propertyValueReferenceValidationSchema} = require('./../reference-validation-schemas/property-value.reference-validation-schema'),
  {searchActionReferenceValidationSchema} = require('./../reference-validation-schemas/search-action.reference-validation-schema'),
  {textValidationSchema} = require('./text.validation-schema'),
  {urlValidationSchema} = require('./url.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),

  thingValidationSchema = validationSchemaModel(
    'Thing',
    {
      additionalType: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema)
      ),
      alternateName: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema)
      ),
      description: textValidationSchema,
      disambiguatingDescription: textValidationSchema,
      identifier: Joi.alternatives(
        textValidationSchema,
        propertyValueReferenceValidationSchema
      ).required(),
      image: Joi.alternatives(
        urlValidationSchema,
        Joi.array().items(urlValidationSchema),
        imageObjectReferenceValidationSchema,
        Joi.array().items(imageObjectReferenceValidationSchema),
        Joi.array().items(Joi.array().items(imageObjectReferenceValidationSchema))
      ),
      mainEntityOfPage: Joi.alternatives(
        urlValidationSchema,
        creativeWorkReferenceValidationSchema
      ),
      name: textValidationSchema,
      potentialAction: Joi.alternatives(
        actionReferenceValidationSchema,
        Joi.array().items(actionReferenceValidationSchema),
        searchActionReferenceValidationSchema,
        Joi.array().items(searchActionReferenceValidationSchema)
      ).allow(null),
      sameAs: urlValidationSchema,
      subjectOf: Joi.alternatives(
        creativeWorkReferenceValidationSchema,
        Joi.array().items(creativeWorkReferenceValidationSchema),
        eventReferenceValidationSchema,
        Joi.array().items(eventReferenceValidationSchema)
      ).allow(null),
      url: urlValidationSchema
    }
  );

module.exports = {
  thingValidationSchema
};
