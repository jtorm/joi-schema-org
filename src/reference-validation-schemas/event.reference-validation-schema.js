/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  eventReferenceValidationSchema = referenceValidationSchemaModel('Event');

module.exports = {
  eventReferenceValidationSchema
};
