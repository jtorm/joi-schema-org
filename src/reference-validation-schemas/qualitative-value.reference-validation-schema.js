/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  qualitativeValueReferenceValidationSchema = referenceValidationSchemaModel('QualitativeValue');

module.exports = {
  qualitativeValueReferenceValidationSchema
};
