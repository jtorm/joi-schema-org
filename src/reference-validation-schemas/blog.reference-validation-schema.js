/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  blogReferenceValidationSchema = referenceValidationSchemaModel('Blog');

module.exports = {
  blogReferenceValidationSchema
};
