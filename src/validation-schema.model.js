/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  validationSchemaModel = function(type, obj) {
    const resultObj = {
      "@context": Joi.string().uri(),
      "@type": Joi.string().pattern(new RegExp('^' + type + '$')).required(),
      '@id': Joi.string().uri(),
      'id': Joi.string()
    };

    if (typeof obj === 'object') {
      return {...obj, ...resultObj};
    }

    return resultObj;
  };

module.exports = {
  validationSchemaModel
};
