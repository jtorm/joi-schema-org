/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  {definedTermReferenceValidationSchema} = require('./../reference-validation-schemas/defined-term.reference-validation-schema'),
  {enumerationReferenceValidationSchema} = require('./../reference-validation-schemas/enumeration.reference-validation-schema'),
  {measurementTypeEnumerationValidationSchema} = require('./measurement-type-enumeration.validation-schema'),
  {propertyValueReferenceValidationSchema} = require("./../reference-validation-schemas/property-value.reference-validation-schema"),
  {qualitativeValueReferenceValidationSchema} = require('./../reference-validation-schemas/qualitative-value.reference-validation-schema'),
  {quantitativeValueReferenceValidationSchema} = require('./../reference-validation-schemas/quantitative-value.reference-validation-schema'),
  {structuredValueReferenceValidationSchema} = require('./../reference-validation-schemas/structured-value.reference-validation-schema'),
  {textValidationSchema} = require('./text.validation-schema'),
  {thingValidationSchema} = require('./thing.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),

  qualitativeValueValidationSchema = validationSchemaModel(
    'QualitativeValue',
    {
      "additionalProperty": propertyValueReferenceValidationSchema,
      "equal": qualitativeValueReferenceValidationSchema,
      "greater": qualitativeValueReferenceValidationSchema,
      "greaterOrEqual": qualitativeValueReferenceValidationSchema,
      "lesser": qualitativeValueReferenceValidationSchema,
      "lesserOrEqual": qualitativeValueReferenceValidationSchema,
      "nonEqual": qualitativeValueReferenceValidationSchema,
      "valueReference": Joi.alternatives(
        definedTermReferenceValidationSchema,
        enumerationReferenceValidationSchema,
        measurementTypeEnumerationValidationSchema,
        propertyValueReferenceValidationSchema,
        qualitativeValueReferenceValidationSchema,
        quantitativeValueReferenceValidationSchema,
        structuredValueReferenceValidationSchema,
        textValidationSchema
      ),

      ...thingValidationSchema
    }
  );

module.exports = {
  qualitativeValueValidationSchema
};
