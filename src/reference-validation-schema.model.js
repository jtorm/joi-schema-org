/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  {languageReferenceValidationSchema} = require("./reference-validation-schemas/language.reference-validation-schema"),
  {propertyValueReferenceValidationSchema} = require('./reference-validation-schemas/property-value.reference-validation-schema'),
  {textValidationSchema} = require("./validation-schemas/text.validation-schema"),
  {validationSchemaModel} = require('./validation-schema.model'),

  referenceValidationSchemaModel = function (type) {
    return validationSchemaModel(
      type,
      {
        identifier: Joi.alternatives(
          Joi.string(),
          propertyValueReferenceValidationSchema,
        ).required(),
        inLanguage: Joi.alternatives(
          textValidationSchema,
          languageReferenceValidationSchema
        )
      }
    );
  };

module.exports = {
  referenceValidationSchemaModel
};
