/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  structuredValueReferenceValidationSchema = referenceValidationSchemaModel('StructuredValue');

module.exports = {
  structuredValueReferenceValidationSchema
};
