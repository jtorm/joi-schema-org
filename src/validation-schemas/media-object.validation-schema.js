/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  {booleanValidationSchema} = require('./boolean.validation-schema'),
  {claimReferenceValidationSchema} = require('./../reference-validation-schemas/claim.reference-validation-schema'),
  {anyThingReferenceValidationSchema} = require('./../reference-validation-schemas/any-thing.reference-validation-schema'),
  {creativeWorkValidationSchema} = require('./creative-work.validation-schema'),
  {dateValidationSchema} = require('./date.validation-schema'),
  {dateTimeValidationSchema} = require('./date-time.validation-schema'),
  {distanceReferenceValidationSchema} = require('./../reference-validation-schemas/distance.reference-validation-schema'),
  {durationReferenceValidationSchema} = require('./../reference-validation-schemas/duration.reference-validation-schema'),
  {geoShapeReferenceValidationSchema} = require('./../reference-validation-schemas/geo-shape.reference-validation-schema'),
  {newsArticleReferenceValidationSchema} = require('./../reference-validation-schemas/news-article.reference-validation-schema'),
  {mediaSubscriptionReferenceValidationSchema} = require('./../reference-validation-schemas/media-subscription.reference-validation-schema'),
  {organizationReferenceValidationSchema} = require('./../reference-validation-schemas/organization.reference-validation-schema'),
  {placeReferenceValidationSchema} = require("./../reference-validation-schemas/place.reference-validation-schema"),
  {timeValidationSchema} = require('./time.validation-schema'),
  {textValidationSchema} = require('./text.validation-schema'),
  {quantitativeValueReferenceValidationSchema} = require('./../reference-validation-schemas/quantitative-value.reference-validation-schema'),
  {urlValidationSchema} = require('./url.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),

  mediaObjectValidationSchema = validationSchemaModel(
    'MediaObject',
    {
      associatedArticle: newsArticleReferenceValidationSchema,
      bitrate: textValidationSchema,
      contentSize: textValidationSchema,
      contentUrl: urlValidationSchema,
      duration: durationReferenceValidationSchema,
      embedUrl: urlValidationSchema,
      encodesCreativeWork: anyThingReferenceValidationSchema,
      encodingFormat: Joi.alternatives(
        textValidationSchema,
        urlValidationSchema
      ),
      endTime: Joi.alternatives(
        dateTimeValidationSchema,
        timeValidationSchema
      ),
      height: Joi.alternatives(
        distanceReferenceValidationSchema,
        quantitativeValueReferenceValidationSchema
      ).allow(null),
      ineligibleRegion: Joi.alternatives(
        geoShapeReferenceValidationSchema,
        Joi.array().items(geoShapeReferenceValidationSchema),
        placeReferenceValidationSchema,
        Joi.array().items(placeReferenceValidationSchema),
        textValidationSchema,
        Joi.array().items(textValidationSchema)
      ),
      interpretedAsClaim: Joi.alternatives(
        claimReferenceValidationSchema,
        Joi.array().items(claimReferenceValidationSchema)
      ).allow(null),
      playerType: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema)
      ),
      productionCompany: Joi.alternatives(
        organizationReferenceValidationSchema,
        Joi.array().items(organizationReferenceValidationSchema)
      ).allow(null),
      regionsAllowed: Joi.alternatives(
        placeReferenceValidationSchema,
        Joi.array().items(placeReferenceValidationSchema)
      ).allow(null),
      requiresSubscription: Joi.alternatives(
        booleanValidationSchema,
        mediaSubscriptionReferenceValidationSchema
      ),
      sha256: textValidationSchema,
      startTime: Joi.alternatives(
        dateTimeValidationSchema,
        timeValidationSchema
      ),
      uploadDate: dateValidationSchema,
      width: Joi.alternatives(
        distanceReferenceValidationSchema,
        quantitativeValueReferenceValidationSchema
      ).allow(null),

      ...creativeWorkValidationSchema
    }
  );

module.exports = {
  mediaObjectValidationSchema
};
