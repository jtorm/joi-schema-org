/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  {booleanValidationSchema} = require('./boolean.validation-schema'),
  {imageObjectReferenceValidationSchema} = require('./../reference-validation-schemas/image-object.reference-validation-schema'),
  {mediaObjectValidationSchema} = require('./media-object.validation-schema'),
  {mediaObjectReferenceValidationSchema} = require('./../reference-validation-schemas/media-object.reference-validation-schema'),
  {propertyValueReferenceValidationSchema} = require('./../reference-validation-schemas/property-value.reference-validation-schema'),
  {textValidationSchema} = require('./text.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),

  imageObjectValidationSchema = validationSchemaModel(
    'ImageObject',
    {
      caption: Joi.alternatives(
        mediaObjectReferenceValidationSchema,
        textValidationSchema
      ),
      embeddedTextCaption: textValidationSchema,
      exifData: Joi.alternatives(
        propertyValueReferenceValidationSchema,
        Joi.array().items(propertyValueReferenceValidationSchema),
        textValidationSchema,
        Joi.array().items(textValidationSchema)
      ),
      representativeOfPage: booleanValidationSchema,
      thumbnail: Joi.alternatives(imageObjectReferenceValidationSchema).allow(null),

      ...mediaObjectValidationSchema
    }
  );

module.exports = {
  imageObjectValidationSchema
};
