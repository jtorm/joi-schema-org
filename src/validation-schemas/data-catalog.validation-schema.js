/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  {creativeWorkValidationSchema} = require('./creative-work.validation-schema'),
  {datasetReferenceValidationSchema} = require('./../reference-validation-schemas/dataset.reference-validation-schema'),
  {textValidationSchema} = require('./text.validation-schema'),
  {urlValidationSchema} = require('./url.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),

  dataCatalogValidationSchema = validationSchemaModel(
    'DataCatalog',
    {
      dataset: Joi.alternatives(datasetReferenceValidationSchema).allow(null),
      measurementTechnique: Joi.alternatives(
        textValidationSchema,
        urlValidationSchema
      ),

      ...creativeWorkValidationSchema
    }
  );

module.exports = {
  dataCatalogValidationSchema
};
