/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  aggregateRatingReferenceValidationSchema = referenceValidationSchemaModel('AggregateRating');

module.exports = {
  aggregateRatingReferenceValidationSchema
};
