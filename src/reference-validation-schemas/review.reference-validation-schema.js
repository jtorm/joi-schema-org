/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  reviewReferenceValidationSchema = referenceValidationSchemaModel('Review');

module.exports = {
  reviewReferenceValidationSchema
};
