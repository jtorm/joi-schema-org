/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  programMembershipReferenceValidationSchema = referenceValidationSchemaModel('ProgramMembership');

module.exports = {
  programMembershipReferenceValidationSchema
};
