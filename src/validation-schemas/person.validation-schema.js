/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  {anyThingReferenceValidationSchema} = require("./../reference-validation-schemas/any-thing.reference-validation-schema"),
  {brandReferenceValidationSchema} = require('./../reference-validation-schemas/brand.reference-validation-schema'),
  {contactPointReferenceValidationSchema} = require('./../reference-validation-schemas/contact-point.reference-validation-schema'),
  {countryReferenceValidationSchema} = require("./../reference-validation-schemas/country.reference-validation-schema"),
  {creativeWorkReferenceValidationSchema} = require('./../reference-validation-schemas/creative-work.reference-validation-schema'),
  {dateValidationSchema} = require('./date.validation-schema'),
  {definedTermReferenceValidationSchema} = require('./../reference-validation-schemas/defined-term.reference-validation-schema'),
  {demandReferenceValidationSchema} = require('./../reference-validation-schemas/demand.reference-validation-schema'),
  {distanceReferenceValidationSchema} = require('./../reference-validation-schemas/distance.reference-validation-schema'),
  {educationalOccupationalCredentialReferenceValidationSchema} = require('./../reference-validation-schemas/educational-occupational-credential.reference-validation-schema'),
  {educationalOrganizationReferenceValidationSchema} = require('./../reference-validation-schemas/educational-organization.reference-validation-schema'),
  {eventReferenceValidationSchema} = require('./../reference-validation-schemas/event.reference-validation-schema'),
  {genderTypeValidationSchema} = require('./gender-type.validation-schema'),
  {grantReferenceValidationSchema} = require('./../reference-validation-schemas/grant.reference-validation-schema'),
  {interactionCounterReferenceValidationSchema} = require('./../reference-validation-schemas/interaction-counter.reference-validation-schema'),
  {languageReferenceValidationSchema} = require('./../reference-validation-schemas/language.reference-validation-schema'),
  {monetaryAmountReferenceValidationSchema} = require('./../reference-validation-schemas/monetary-amount.reference-validation-schema'),
  {occupationReferenceValidationSchema} = require('./../reference-validation-schemas/occupation.reference-validation-schema'),
  {offerCatalogReferenceValidationSchema} = require('./../reference-validation-schemas/offer-catalog.reference-validation-schema'),
  {offerReferenceValidationSchema} = require('./../reference-validation-schemas/offer.reference-validation-schema'),
  {organizationReferenceValidationSchema} = require('./../reference-validation-schemas/organization.reference-validation-schema'),
  {ownershipInfoReferenceValidationSchema} = require('./../reference-validation-schemas/ownership-info.reference-validation-schema'),
  {personReferenceValidationSchema} = require('./../reference-validation-schemas/person.reference-validation-schema'),
  {placeReferenceValidationSchema} = require('./../reference-validation-schemas/place.reference-validation-schema'),
  {postalAddressReferenceValidationSchema} = require('./../reference-validation-schemas/postal-address.reference-validation-schema'),
  {priceSpecificationReferenceValidationSchema} = require('./../reference-validation-schemas/price-specification.reference-validation-schema'),
  {productReferenceValidationSchema} = require('./../reference-validation-schemas/product.reference-validation-schema'),
  {programMembershipReferenceValidationSchema} = require('./../reference-validation-schemas/program-membership.reference-validation-schema'),
  {quantitativeValueReferenceValidationSchema} = require('./../reference-validation-schemas/quantitative-value.reference-validation-schema'),
  {textValidationSchema} = require('./text.validation-schema'),
  {thingValidationSchema} = require('./thing.validation-schema'),
  {urlValidationSchema} = require('./url.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),

  personValidationSchema = validationSchemaModel(
    'Person',
    {
      additionalName: textValidationSchema,
      address: Joi.alternatives(
        postalAddressReferenceValidationSchema,
        textValidationSchema
      ),
      affiliation: Joi.alternatives(
        organizationReferenceValidationSchema,
        Joi.array().items(organizationReferenceValidationSchema)
      ).allow(null),
      alumniOf: Joi.alternatives(
        educationalOrganizationReferenceValidationSchema,
        Joi.array().items(educationalOrganizationReferenceValidationSchema),
        organizationReferenceValidationSchema,
        Joi.array().items(organizationReferenceValidationSchema)
      ).allow(null),
      award: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema)
      ),
      birthDate: dateValidationSchema,
      birthPlace: Joi.alternatives(placeReferenceValidationSchema).allow(null),
      brand: Joi.alternatives(
        brandReferenceValidationSchema,
        Joi.array().items(brandReferenceValidationSchema),
        organizationReferenceValidationSchema,
        Joi.array().items(organizationReferenceValidationSchema)
      ).allow(null),
      callSign: textValidationSchema,
      children: Joi.alternatives(
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema)
      ).allow(null),
      colleague: Joi.alternatives(
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema),
        urlValidationSchema,
        Joi.array().items(urlValidationSchema)
      ),
      contactPoint: Joi.alternatives(
        contactPointReferenceValidationSchema,
        Joi.array().items(contactPointReferenceValidationSchema)
      ).allow(null),
      deathDate: dateValidationSchema,
      deathPlace: Joi.alternatives(placeReferenceValidationSchema).allow(null),
      duns: textValidationSchema,
      email: textValidationSchema,
      familyName: textValidationSchema,
      faxNumber: textValidationSchema,
      follows: Joi.alternatives(
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema)
      ).allow(null),
      funder: Joi.alternatives(
        organizationReferenceValidationSchema,
        Joi.array().items(organizationReferenceValidationSchema),
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema)
      ).allow(null),
      funding: Joi.alternatives(
        grantReferenceValidationSchema,
        Joi.array().items(grantReferenceValidationSchema)
      ).allow(null),
      gender: Joi.alternatives(
        genderTypeValidationSchema,
        textValidationSchema
      ),
      givenName: textValidationSchema,
      globalLocationNumber: textValidationSchema,
      hasCredential: Joi.alternatives(
        educationalOccupationalCredentialReferenceValidationSchema,
        Joi.array().items(educationalOccupationalCredentialReferenceValidationSchema)
      ).allow(null),
      hasOccupation: Joi.alternatives(
        occupationReferenceValidationSchema,
        Joi.array().items(occupationReferenceValidationSchema)
      ).allow(null),
      hasOfferCatalog: Joi.alternatives(
        offerCatalogReferenceValidationSchema,
        Joi.array().items(offerCatalogReferenceValidationSchema)
      ).allow(null),
      hasPOS: Joi.alternatives(
        placeReferenceValidationSchema,
        Joi.array().items(placeReferenceValidationSchema)
      ).allow(null),
      height: Joi.alternatives(
        distanceReferenceValidationSchema,
        quantitativeValueReferenceValidationSchema
      ).allow(null),
      homeLocation: Joi.alternatives(
        contactPointReferenceValidationSchema,
        placeReferenceValidationSchema
      ).allow(null),
      honorificPrefix: textValidationSchema,
      honorificSuffix: textValidationSchema,
      interactionStatistic: Joi.alternatives(
        interactionCounterReferenceValidationSchema,
        Joi.array().items(interactionCounterReferenceValidationSchema)
      ).allow(null),
      isicV4: textValidationSchema,
      jobTitle: Joi.alternatives(
        definedTermReferenceValidationSchema,
        textValidationSchema
      ),
      knowsAbout: Joi.alternatives(
        textValidationSchema,
        Joi.array().items(textValidationSchema),
        anyThingReferenceValidationSchema,
        Joi.array().items(anyThingReferenceValidationSchema),
        urlValidationSchema,
        Joi.array().items(urlValidationSchema)
      ),
      knowsLanguage: Joi.alternatives(
        languageReferenceValidationSchema,
        Joi.array().items(languageReferenceValidationSchema),
        textValidationSchema,
        Joi.array().items(textValidationSchema)
      ),
      makesOffer: Joi.alternatives(
        offerReferenceValidationSchema,
        Joi.array().items(offerReferenceValidationSchema)
      ).allow(null),
      memberOf: Joi.alternatives(
        organizationReferenceValidationSchema,
        Joi.array().items(organizationReferenceValidationSchema),
        programMembershipReferenceValidationSchema,
        Joi.array().items(programMembershipReferenceValidationSchema)
      ).allow(null),
      naics: textValidationSchema,
      nationality: Joi.alternatives(countryReferenceValidationSchema).allow(null),
      netWorth: Joi.alternatives(
        monetaryAmountReferenceValidationSchema,
        priceSpecificationReferenceValidationSchema
      ).allow(null),
      owns: Joi.alternatives(
        ownershipInfoReferenceValidationSchema,
        Joi.array().items(ownershipInfoReferenceValidationSchema),
        productReferenceValidationSchema,
        Joi.array().items(productReferenceValidationSchema)
      ).allow(null),
      parent: Joi.alternatives(
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema)
      ).allow(null),
      performerIn: Joi.alternatives(
        eventReferenceValidationSchema,
        Joi.array().items(eventReferenceValidationSchema)
      ).allow(null),
      publishingPrinciples: Joi.alternatives(
        creativeWorkReferenceValidationSchema,
        urlValidationSchema
      ),
      relatedTo: Joi.alternatives(
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema)
      ).allow(null),
      seeks: Joi.alternatives(
        demandReferenceValidationSchema,
        Joi.array().items(demandReferenceValidationSchema)
      ).allow(null),
      sibling: Joi.alternatives(
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema)
      ).allow(null),
      sponsor: Joi.alternatives(
        organizationReferenceValidationSchema,
        Joi.array().items(organizationReferenceValidationSchema),
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema)
      ).allow(null),
      spouse: Joi.alternatives(
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema)
      ).allow(null),
      taxID: textValidationSchema,
      telephone: textValidationSchema,
      vatID: textValidationSchema,
      weight: Joi.alternatives(quantitativeValueReferenceValidationSchema).allow(null),
      workLocation: Joi.alternatives(
        contactPointReferenceValidationSchema,
        Joi.array().items(contactPointReferenceValidationSchema),
        placeReferenceValidationSchema,
        Joi.array().items(placeReferenceValidationSchema)
      ).allow(null),
      worksFor: Joi.alternatives(
        organizationReferenceValidationSchema,
        Joi.array().items(organizationReferenceValidationSchema)
      ).allow(null),

      ...thingValidationSchema
    }
  );

module.exports = {
  personValidationSchema
};
