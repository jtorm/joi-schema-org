/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  webPageElementReferenceValidationSchema = referenceValidationSchemaModel('WebPageElement');

module.exports = {
  webPageElementReferenceValidationSchema
};
