/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  itemListReferenceValidationSchema = referenceValidationSchemaModel('ItemList');

module.exports = {
  itemListReferenceValidationSchema
};
