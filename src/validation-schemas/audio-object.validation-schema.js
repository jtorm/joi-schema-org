/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  {mediaObjectReferenceValidationSchema} = require('./../reference-validation-schemas/media-object.reference-validation-schema'),
  {textValidationSchema} = require('./text.validation-schema'),
  {thingValidationSchema} = require('./thing.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),

  audioObjectValidationSchema = validationSchemaModel(
    'AudioObject',
    {
      caption: Joi.alternatives(
        textValidationSchema,
        mediaObjectReferenceValidationSchema
      ),
      embeddedTextCaption: textValidationSchema,
      transcript: textValidationSchema,

      ...thingValidationSchema
    }
  );

module.exports = {
  audioObjectValidationSchema
};
