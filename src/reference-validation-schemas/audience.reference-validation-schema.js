/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  audienceReferenceValidationSchema = referenceValidationSchemaModel('Audience');

module.exports = {
  audienceReferenceValidationSchema
};
