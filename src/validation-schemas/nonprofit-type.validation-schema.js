/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  {textValidationSchema} = require('./text.validation-schema'),
  {urlValidationSchema} = require('./url.validation-schema'),

  nonprofitTypeValidationSchema = Joi.alternatives(
    textValidationSchema,
    urlValidationSchema
  );

module.exports = {
  nonprofitTypeValidationSchema
};
