/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {creativeWorkValidationSchema} = require('./creative-work.validation-schema'),
  {cssSelectorTypeValidationSchema} = require('./css-selector-type.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),
  {xPathTypeValidationSchema} = require('./x-path-type.validation-schema'),

  webPageElementValidationSchema = validationSchemaModel(
    'WebPageElement',
    {
      cssSelector: cssSelectorTypeValidationSchema,
      xpath: xPathTypeValidationSchema,

      ...creativeWorkValidationSchema
    }
  );

module.exports = {
  webPageElementValidationSchema
};
