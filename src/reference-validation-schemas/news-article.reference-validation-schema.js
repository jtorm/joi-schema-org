/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  newsArticleReferenceValidationSchema = referenceValidationSchemaModel('NewsArticle');

module.exports = {
  newsArticleReferenceValidationSchema
};
