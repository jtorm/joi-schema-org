/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  priceSpecificationReferenceValidationSchema = referenceValidationSchemaModel('PriceSpecification');

module.exports = {
  priceSpecificationReferenceValidationSchema
};
