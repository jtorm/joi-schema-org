/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  dataCatalogReferenceValidationSchema = referenceValidationSchemaModel('DataCatalog');

module.exports = {
  dataCatalogReferenceValidationSchema
};
