/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  definedTermReferenceValidationSchema = referenceValidationSchemaModel('DefinedTerm');

module.exports = {
  definedTermReferenceValidationSchema
};
