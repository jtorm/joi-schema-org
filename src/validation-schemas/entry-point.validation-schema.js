/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  {digitalPlatformEnumerationValidationSchema} = require('./digital-platform-enumeration.validation-schema'),
  {softwareApplicationReferenceValidationSchema} = require('./../reference-validation-schemas/software-application.reference-validation-schema'),
  {textValidationSchema} = require('./text.validation-schema'),
  {thingValidationSchema} = require('./thing.validation-schema'),
  {urlValidationSchema} = require('./url.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),

  entryPointValidationSchema = validationSchemaModel(
    'EntryPoint',
    {
      actionApplication: softwareApplicationReferenceValidationSchema,
      actionPlatform: Joi.alternatives(
        digitalPlatformEnumerationValidationSchema,
        textValidationSchema,
        urlValidationSchema
      ),
      contentType: textValidationSchema,
      encodingType: textValidationSchema,
      httpMethod: textValidationSchema,
      urlTemplate: textValidationSchema,

      ...thingValidationSchema
    }
  );

module.exports = {
  entryPointValidationSchema
};
