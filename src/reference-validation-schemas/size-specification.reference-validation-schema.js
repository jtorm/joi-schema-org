/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  sizeSpecificationReferenceValidationSchema = referenceValidationSchemaModel('SizeSpecification');

module.exports = {
  sizeSpecificationReferenceValidationSchema
};
