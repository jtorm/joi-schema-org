/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  clipReferenceValidationSchema = referenceValidationSchemaModel('Clip');

module.exports = {
  clipReferenceValidationSchema
};
