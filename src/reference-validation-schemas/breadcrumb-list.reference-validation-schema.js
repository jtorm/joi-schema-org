/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  {referenceValidationSchemaModel} = require('./../reference-validation-schema.model'),

  breadcrumbListReferenceValidationSchema = referenceValidationSchemaModel('BreadcrumbList');

module.exports = {
  breadcrumbListReferenceValidationSchema
};
