/*! (c) jTorm and other contributors | www.jtorm.com/license */

const
  Joi = require('joi'),

  {breadcrumbListReferenceValidationSchema} = require('./../reference-validation-schemas/breadcrumb-list.reference-validation-schema'),
  {creativeWorkValidationSchema} = require('./creative-work.validation-schema'),
  {dateValidationSchema} = require('./date.validation-schema'),
  {imageObjectReferenceValidationSchema} = require('./../reference-validation-schemas/image-object.reference-validation-schema'),
  {organizationReferenceValidationSchema} = require("./../reference-validation-schemas/organization.reference-validation-schema"),
  {personReferenceValidationSchema} = require("./../reference-validation-schemas/person.reference-validation-schema"),
  {speakableSpecificationReferenceValidationSchema} = require('./../reference-validation-schemas/speakable-specification.reference-validation-schema'),
  {specialtyReferenceValidationSchema} = require('./../reference-validation-schemas/specialty.reference-validation-schema'),
  {textValidationSchema} = require('./text.validation-schema'),
  {urlValidationSchema} = require('./url.validation-schema'),
  {validationSchemaModel} = require('./../validation-schema.model'),
  {webPageElementReferenceValidationSchema} = require('./../reference-validation-schemas/web-page-element.reference-validation-schema'),

  webPageValidationSchema = validationSchemaModel(
    'WebPage',
    {
      breadcrumb: Joi.alternatives(
        breadcrumbListReferenceValidationSchema,
        textValidationSchema
      ).allow(null),
      lastReviewed: Joi.alternatives(dateValidationSchema).allow(null),
      mainContentOfPage: Joi.alternatives(webPageElementReferenceValidationSchema).allow(null),
      primaryImageOfPage: Joi.alternatives(imageObjectReferenceValidationSchema).allow(null),
      relatedLink: Joi.alternatives(
        urlValidationSchema,
        Joi.array().items(urlValidationSchema)
      ).allow(null),
      reviewedBy: Joi.alternatives(
        personReferenceValidationSchema,
        Joi.array().items(personReferenceValidationSchema),
        organizationReferenceValidationSchema,
        Joi.array().items(organizationReferenceValidationSchema)
      ).allow(null),
      significantLink: Joi.alternatives(
        urlValidationSchema,
        Joi.array().items(urlValidationSchema)
      ).allow(null),
      speakable: Joi.alternatives(
        speakableSpecificationReferenceValidationSchema,
        Joi.array().items(speakableSpecificationReferenceValidationSchema),
        urlValidationSchema,
        Joi.array().items(urlValidationSchema)
      ),
      specialty: Joi.alternatives(
        specialtyReferenceValidationSchema,
        Joi.array().items(specialtyReferenceValidationSchema)
      ).allow(null),

      ...creativeWorkValidationSchema
    }
  );

module.exports = {
  webPageValidationSchema
};
